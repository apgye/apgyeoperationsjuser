library(testthat)

library(apgyeOperationsJusER)
leerArchivoTest <- function(fileName, isDBF) {
  realPath <- if(file.exists(fileName)) fileName
  else paste('tests/testthat/', fileName, sep='')

  apgyeOperationsJusER::leerArchivoPresentado(realPath, isDBF)
}
test_check("apgyeOperationsJusER")
