library(testthat)
library(apgyeCommon)
library(lubridate)
context("test-citipm.R")
flog.threshold(DEBUG)

test_that("CITIPM : warnings", {

  # Setup
  data_interval <- interval(ymd("2017-11-01"), ymd("2017-12-01"))
  op <- CITIPM$new(data_interval)
  df <- leerArchivoTest('CITIPM-Nov17-muistj0100pna.txt', FALSE)
  input <- list(tbs_prim_CITIPM=df)

  # Method under test
  result <- op$process(input)

  # Verification warnings
  expect_length(result$warnings, 0)
  # Verification result
  expect_equal(nrow(result$results$result), 93)
})

test_that("CITIPM : emptyDF", {

  # Setup
  data_interval <- interval(ymd("2017-11-01"), ymd("2017-12-01"))
  op <- CITIPM$new(data_interval)
  df <- data.frame()
  input <- list(tbs_prim_CITIPM=df)

  # Method under test
  result <- op$process(input)

  # Verification warnings
  expect_length(result$warnings, 0)

  # Verification result
  expect_length(result$results$result, 0)
})

