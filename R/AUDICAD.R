# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 26-06-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @import dplyr
#' @import apgyeCommon
#' @export
AUDICAD <- R6::R6Class("AUDICAD",
                    inherit = Operation,
                    private = list(
                      processInput = function( tbs_prim_AUDICAD ) {
                        result <- OperationResult$new()
                        if (nrow(tbs_prim_AUDICAD) == 0){
                          df <- data.frame()
                          attr(df, "empty") <- TRUE
                          result$addResult(df)

                          return(result)
                        }

                        df <- data.frame()

                        colnames(tbs_prim_AUDICAD) <- try(c("nro", "caratula", "tproc",
                                                             "finicio", "duracm",
                                                             "duracb", "ffaud", "fraud",
                                                             "estaud", "mact"))

                        calidad_primaria <- vector()

                        realizadas <- !is.na(tbs_prim_AUDICAD$estaud) &
                                  as.integer(tbs_prim_AUDICAD$estaud) == 2

                        a <- tbs_prim_AUDICAD$estaud %in% c(2:5)
                        if (checkWarn(all(a))){
                          e_vector <- tbs_prim_AUDICAD %>%
                            filter(!a) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "El Tipo de Audiencia debe estar
                            comprendido entre 2 y 5. Causas involucradas: "
                          result$addError("Tipo de Audiencia", paste0(mensaje, erroneas))
                        }
                        c <- tbs_prim_AUDICAD %>%
                          filter(realizadas) %>%
                          .$duracm %>% as.integer()
                        c <- c/c # para una calidad primaria correcta
                        if (anyNA(c)){
                          e_vector <- tbs_prim_AUDICAD %>%
                            filter(realizadas) %>%
                            filter(is.na(c)) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Para las Audiencias realizadas en el período, la
                          Duración en minutos debe ser un número entero mayor
                          a cero. Causas involucradas: "
                          result$addError("Duración en Minutos", paste0(mensaje, erroneas))
                        }

                        d <- tbs_prim_AUDICAD %>%
                          filter(realizadas) %>%
                          .$duracb %>% as.integer()
                        d <- d/d # para una calidad primaria correcta
                        if (anyNA(d)){
                          e_vector <- tbs_prim_AUDICAD %>%
                            filter(realizadas) %>%
                            filter(is.na(d)) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Para las Audiencias realizadas en el período, la
                          Cantidad de Bloques debe ser un número entero mayor
                          a cero. Causas involucradas: "
                          result$addError("Cantidad de Bloques", paste0(mensaje, erroneas))
                        }

                        e <- !is.na(tbs_prim_AUDICAD$mact[realizadas])
                        if (checkWarn(all(e))){
                          e_vector <- tbs_prim_AUDICAD[realizadas] %>%
                            filter(!e) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Existen audiencias sin declarar el
                            Funcionario. Causas involucradas: "
                          result$addWarning("Funcionario", paste0(mensaje, erroneas))
                        }
                        # Validacion Fechas
                        f <- !is.na(tbs_prim_AUDICAD$ffaud)
                        if (checkWarn(all(f))){
                          e_vector <- tbs_prim_AUDICAD %>%
                            filter(!f) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Existen Audiencias sin Fecha
                            de Fijación cargada. Causas involucradas: "
                          result$addError("Fecha Fijación", paste0(mensaje, erroneas))
                        }
                        # TODO: Validar cuando estado distinto '1' ó !na la fecha
                        # de efectivizacion no sea nula/vacia
                        g <- !is.na(tbs_prim_AUDICAD$fraud[!is.na(tbs_prim_AUDICAD$estaud) &
                                                  as.integer(tbs_prim_AUDICAD$estaud) != 1])
                        if (checkWarn(all(g))){
                          e_vector <- tbs_prim_AUDICAD[!is.na(tbs_prim_AUDICAD$estaud) &
                                              as.integer(tbs_prim_AUDICAD$estaud) != 1] %>%
                            filter(!g) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Existen Audiencias sin Fecha
                            de Efectivización cargada. Causas involucradas: "
                          result$addError("Fecha Efectivización", paste0(mensaje, erroneas))
                        }
                        tbs_prim_AUDICAD$ffaud <- as.Date(tbs_prim_AUDICAD$ffaud, format="%d/%m/%Y")
                        tbs_prim_AUDICAD$fraud <- as.Date(tbs_prim_AUDICAD$fraud, format="%d/%m/%Y")
                        ambas <- !is.na(tbs_prim_AUDICAD$ffaud) & !is.na(tbs_prim_AUDICAD$fraud)
                        h <- tbs_prim_AUDICAD[ambas, ffaud] <= tbs_prim_AUDICAD[ambas, fraud]
                        if (checkWarn(all(h))){
                          e_vector <- tbs_prim_AUDICAD[ambas] %>%
                            filter(!h) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Existen Causas con Fecha de Fijación mayor
                            a la Fecha de Efectivización. Causas involucradas: "
                          result$addError("Fecha de Fijación mayor", paste0(mensaje, erroneas))
                        }

                        x <- c(a, c, d, e, f, g, h)
                        calidad_primaria <- round(sum(x, na.rm = T)/length(x), 4)

                        # Preparar tbs primarias: Asignar clase a columnas fechas, Agregar columna de
                        # mes de dictado de resolución. Convertir a mayúsculas as
                        tbs_prim_AUDICAD$finicio <-
                          as.Date(tbs_prim_AUDICAD$finicio, format="%d/%m/%Y")
                        tbs_prim_AUDICAD$ffaud <-
                          as.Date(tbs_prim_AUDICAD$ffaud, format="%d/%m/%Y")
                        tbs_prim_AUDICAD$fraud <-
                          as.Date(tbs_prim_AUDICAD$fraud, format="%d/%m/%Y")
                        tbs_prim_AUDICAD$mes_ffaud <-
                          month(tbs_prim_AUDICAD$ffaud)
                        tbs_prim_AUDICAD$mes_fraud <-
                          month(tbs_prim_AUDICAD$fraud)
                        tbs_prim_AUDICAD$duracm <-
                          as.integer(gsub("[^0-9]", "", tbs_prim_AUDICAD$duracm))
                        tbs_prim_AUDICAD$duracb <-
                          as.integer(gsub("[^0-9]", "", tbs_prim_AUDICAD$duracb))

                        # Creación de funciones

                        p_aud_fijadas_total <- function() {
                          # Cuenta audiencias informadas (total)
                          aud_fijadas_total <- nrow(tbs_prim_AUDICAD)
                          aud_fijadas_total
                        }

                        p_aud_fijadas <- function() {
                          # Cuenta audiencias fijadas en el mes
                          aud_fijadas <- sum(self$withinInterval(tbs_prim_AUDICAD$ffaud), na.rm = T)
                          aud_fijadas

                        }

                        p_aud_realizadas <- function() {
                          # Cuenta audiencias realizadas en el mes
                          aud_realizadas <- sum(self$withinInterval(tbs_prim_AUDICAD$fraud) &
                                                  tbs_prim_AUDICAD$esta == 2, na.rm = T)
                          aud_realizadas

                        }

                        p_aud_norealizadas <- function() {
                          # Cuenta audiencias no realizadas en el mes
                          aud_norealizadas <- sum(self$withinInterval(tbs_prim_AUDICAD$fraud) &
                                                    tbs_prim_AUDICAD$esta == 3, na.rm = T)
                          aud_norealizadas

                        }

                        p_aud_canceladas <- function() {
                          # Cuenta audiencias canceladas en el mes
                          aud_canceladas <- sum(self$withinInterval(tbs_prim_AUDICAD$fraud) &
                                                  tbs_prim_AUDICAD$esta == 4, na.rm = T)
                          aud_canceladas

                        }

                        p_aud_reprogramadas <- function() {
                          # Cuenta audiencias reprogramadas en el mes
                          aud_reprogramadas <- sum(self$withinInterval(tbs_prim_AUDICAD$fraud) &
                                                     tbs_prim_AUDICAD$esta == 5, na.rm = T)
                          aud_reprogramadas

                        }

                        p_prom_durac_ffaud_frealizada <- function() {
                          # Duración desde Fecha de Fijación a F.Realización
                          indice_aud_realizadas <- self$withinInterval(tbs_prim_AUDICAD$fraud) &
                            tbs_prim_AUDICAD$esta == 2
                          durac_ffaud_frealizada <-
                            mean(as.integer(tbs_prim_AUDICAD$fraud[indice_aud_realizadas] -
                                              tbs_prim_AUDICAD$ffaud[indice_aud_realizadas]),
                                 na.rm = T)
                          durac_ffaud_frealizada
                        }

                        p_prom_duracm <- function() {
                          # Promedio de duración en minutos por audiencia
                          indice_aud_realizadas <- self$withinInterval(tbs_prim_AUDICAD$fraud) &
                            tbs_prim_AUDICAD$esta == 2
                          prom_duracm <- mean(tbs_prim_AUDICAD$duracm[indice_aud_realizadas],
                                              na.rm = T)
                          prom_duracm
                        }

                        p_mediana_duracm <- function() {
                          # Mediana de duración en minutos por audiencia
                          indice_aud_realizadas <- self$withinInterval(tbs_prim_AUDICAD$fraud) &
                            tbs_prim_AUDICAD$esta == 2
                          mediana_duracm <- median(tbs_prim_AUDICAD$duracm[indice_aud_realizadas],
                                                   na.rm = T)
                          mediana_duracm
                        }

                        p_prom_duracb <- function() {
                          # Promedio de duración en minutos por audiencia
                          indice_aud_realizadas <- self$withinInterval(tbs_prim_AUDICAD$fraud) &
                            tbs_prim_AUDICAD$esta == 2
                          prom_duracb <- mean(tbs_prim_AUDICAD$duracb[indice_aud_realizadas],
                                              na.rm = T)
                          prom_duracb
                        }


                        # DESARROLLO:
                        # Audiencias y tipo de proceso!

                        # crear vectores
                        aud_fijadas_total <- vector()
                        aud_fijadas <- vector()
                        aud_realizadas <- vector()
                        aud_norealizadas <- vector()
                        aud_canceladas <- vector()
                        aud_reprogramadas <- vector()
                        durac_ffaud_frealizada <- vector()
                        prom_duracm <- vector()
                        mediana_duracm <- vector()
                        prom_duracb <- vector()

                        # Extracción de indicadores estadísticos.Con "try" para devolver NA ante
                        # falla
                        aud_fijadas_total <- try(p_aud_fijadas_total(), silent = TRUE)
                        aud_fijadas_total
                        aud_fijadas <- try(p_aud_fijadas(), silent = TRUE)
                        aud_fijadas
                        aud_realizadas <- try(p_aud_realizadas(), silent = TRUE)
                        aud_realizadas
                        aud_norealizadas <- try(p_aud_norealizadas(), silent = TRUE)
                        aud_norealizadas
                        aud_canceladas <- try(p_aud_canceladas(), silent = TRUE)
                        aud_canceladas
                        aud_reprogramadas <- try(p_aud_reprogramadas(), silent = TRUE)
                        aud_reprogramadas
                        durac_ffaud_frealizada <- try(p_prom_durac_ffaud_frealizada(), silent = TRUE)
                        durac_ffaud_frealizada
                        prom_duracm <- try(p_prom_duracm(), silent = TRUE)
                        prom_duracm
                        mediana_duracm <- try(p_mediana_duracm(), silent = TRUE)
                        mediana_duracm
                        prom_duracb <- try(p_prom_duracb(), silent = TRUE)
                        prom_duracb

                        # Arma df indicadores procesados
                        df <- data.frame(
                          aud_fijadas_total,
                          aud_fijadas,
                          aud_realizadas,
                          aud_norealizadas,
                          aud_canceladas,
                          aud_reprogramadas,
                          durac_ffaud_frealizada,
                          prom_duracm,
                          mediana_duracm,
                          prom_duracb,
                          calidad_primaria)

                        # Evalúa calidad del informe presentado en base a consistencia de indicadores
                        # TODO: CONSULTAR A SEBA POR VALIDACIONES DE CALIDAD EN AUDIENCIAS
                        regla1 <- aud_fijadas == (aud_realizadas
                                                  + aud_norealizadas)

                        regla2 <- aud_norealizadas == (aud_canceladas
                                                       + aud_reprogramadas)

                        df$calidad <- round(sum(regla1, regla2, na.rm = T)/2, 2)

                        result$addResult(df)

                        result
                      }
                    )
)
