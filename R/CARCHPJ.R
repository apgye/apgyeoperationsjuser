# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 05-07-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @import lubridate
#' @importFrom stringr str_c
#' @export
CARCHPJ <- R6::R6Class("CARCHPJ",
                  inherit = Operation,
                  private = list(
                    processInput = function(tbs_prim_CARCHPJ){

                      colnames(tbs_prim_CARCHPJ) <- tolower(colnames(tbs_prim_CARCHPJ))

                      result <- OperationResult$new()
                      if (nrow(tbs_prim_CARCHPJ) == 0){
                        df <- data.frame()
                        attr(df, "empty") <- TRUE
                        result$addResult(df)
                        return(result)
                      }

                      # Asigamos 1 a la Primaria (anteriormente ya se validó la cantidad
                      # de columnas del archivo)
                      calidad_primaria <- 1

                      archivados <- tbs_prim_CARCHPJ %>% nrow()

                      df <- data.frame(
                        calidad_primaria = calidad_primaria,
                        archivados = archivados
                      )

                      result$addResult(df)

                      result

                    }
                  )

)

