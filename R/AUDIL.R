# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 06-02-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero
# Old-Authors: Ing. Federico Caballaro, Lic. Paolo Orundés

## input: DataFrame to Process
## output: BD_AUDIL actualization
## periodicity: On Demand
#' @export
AUDIL <- R6::R6Class("AUDIL",
       inherit = Operation,
       private = list(
       processInput = function(tbs_prim_AUDIL){

        result <- OperationResult$new()
        if (nrow(tbs_prim_AUDIL) == 0){
          df <- data.frame()
          attr(df, "empty") <- TRUE
          result$addResult(df)
          return(result)
        }

        # AJUSTE TEMPORAL HASTA RESOLVER DESDE LA BASE QUE NO GUARDE MAS COMO
        # TRUE LOS CAMPOS QUE VIENEN CON T O FALSE LOS QUE VIENEN CON F
        if (any(tbs_prim_AUDIL$ra %in% c("TRUE","true"))) {
          tbs_prim_AUDIL$ra <- as.character(tbs_prim_AUDIL$ra)
          tbs_prim_AUDIL$ra[tbs_prim_AUDIL$ra %in% c("TRUE","true")] <- "T"
        }

        colnames(tbs_prim_AUDIL) <- try(c("nro",
                                          "caratula",
                                          "tproc",
                                          "asist",
                                          "finicio",
                                          "duracm", #Error-d
                                          "duracb",
                                          "ffa", #Error-g
                                          "fea",
                                          "ta", #Error-a
                                          "ra", #Error-c
                                          "esta", #Error-b
                                          "jaud", #Error-f
                                          "mat",
                                          "justiables"
                                          ))

        calidad_primaria <- vector()

        tbs_prim_AUDIL$ffa <-
          as.Date(tbs_prim_AUDIL$ffa, format="%d/%m/%Y")
        tbs_prim_AUDIL$fea <-
          as.Date(tbs_prim_AUDIL$fea, format="%d/%m/%Y")

        inInterval <- !is.na(tbs_prim_AUDIL$fea) & self$withinInterval(tbs_prim_AUDIL$fea)
        realiz <- !is.na(tbs_prim_AUDIL$esta) & tbs_prim_AUDIL$esta == "2"

        ta_expected <- c("P", "V", "O", "p", "v", "o")
        ra_expected <- c("T", "P", "S", "t", "p", "s")

        a <- tbs_prim_AUDIL %>%
          filter(!is.na(fea)) %>%
          .$ta %in% ta_expected
        if (checkWarn(all(a))){
          e_vector <- tbs_prim_AUDIL %>%
            filter(!is.na(fea)) %>%
            filter(!a) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Existen Audiencias mal declaradas
              según su Clasificación ('P', 'V' u 'O'). Causas involucradas: "
          result$addError("Tipos de Audiencia", paste0(mensaje, erroneas))
        }

        b <- tbs_prim_AUDIL$esta %in% c(1:5)
        if (checkWarn(all(b))){
          e_vector <- tbs_prim_AUDIL %>%
            filter(!b) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El Estado debe estar comprendido entre
                                      1 y 5. Causas involucradas: "
          result$addError("Estado", paste0(mensaje, erroneas))
        }
        c <- tbs_prim_AUDIL %>%
          filter(inInterval & realiz) %>%
          filter(ta %in% c("P", "p")) %>%
          .$ra %in% ra_expected
        if (checkWarn(all(c))){
          e_vector <- tbs_prim_AUDIL %>%
            filter(inInterval & realiz) %>%
            filter(ta %in% c("P", "p")) %>%
            filter(!c) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Existen Audiencias mal declaradas
              según su Clasificación ('T', 'P' ó 'S'). Causas involucradas: "
          result$addError("Resultado", paste0(mensaje, erroneas))
        }
        d <- tbs_prim_AUDIL %>%
          filter(inInterval & realiz) %>%
          .$duracm %>% as.integer()
        d <- d/d # para una calidad primaria correcta
        if (anyNA(d)){
          e_vector <- tbs_prim_AUDIL %>%
            filter(inInterval & realiz) %>%
            filter(is.na(d)) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Para las Audiencias realizadas en el período, la
          Duración en minutos debe ser un número entero mayor
          a cero. Causas involucradas: "
          result$addError("Duración en Minutos", paste0(mensaje, erroneas))
        }

        e <- tbs_prim_AUDIL %>%
          filter(inInterval & realiz) %>%
          .$duracb %>% as.integer()
        e <- e/e # para una calidad primaria correcta
        if (anyNA(e)){
          e_vector <- tbs_prim_AUDIL %>%
            filter(inInterval & realiz) %>%
            filter(is.na(e)) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Para las Audiencias realizadas en el período, la
          Cantidad de Bloques debe ser un número entero mayor
          a cero. Causas involucradas: "
          result$addError("Cantidad de Bloques", paste0(mensaje, erroneas))
        }

        if ((month(int_start(self$interval))+1 == month(Sys.Date()) |
             month(int_start(self$interval)) == 12 & month(Sys.Date()) %in% c(1,2)) & #add by T2328
            exists("id_agentes_gbl")){
          magist_func_id_agentes <- id_agentes_gbl

        } else {
          # buscaremos los id_agentes para el período a presentar. caso Fam2Pna(255-Solari)
          magist_func_id_agentes <- magistrados_y_secretarios_periodo(DB_PROD(),
                                                                      int_start(self$interval),
                                                                      int_end(self$interval)) %>%
            pull(idagente)

          # add "0" ti IDs for External Agent
          magist_func_id_agentes[length(magist_func_id_agentes)+1] <- "0"
        }

        # JAUD VALIDATION
        f <- tbs_prim_AUDIL %>%
          filter(realiz) %>%
          .$jaud %in% magist_func_id_agentes
        if (checkWarn(all(f))){
          e_vector <- tbs_prim_AUDIL %>%
            filter(realiz) %>%
            filter(!f) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Para Audiencias Realizadas -esta = 2-,
            existe/n código/s declarados para el Juez
            no reconocidos o faltantes. Causas involucradas: "
          result$addError("Código del Juez", paste0(mensaje, erroneas))
        }
        # Validacion Fechas
        g <- !is.na(tbs_prim_AUDIL$ffa)
        if (checkWarn(all(g))){
          e_vector <- tbs_prim_AUDIL %>%
            filter(!g) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Existen Audiencias sin Fecha
                            de Fijación cargada. Causas involucradas: "
          result$addError("Fecha Fijación", paste0(mensaje, erroneas))
        }

        h <- !is.na(tbs_prim_AUDIL[!is.na(tbs_prim_AUDIL$esta) &
                                        tbs_prim_AUDIL$esta != "1",fea])
        if (checkWarn(all(h))){
          e_vector <- tbs_prim_AUDIL[!is.na(tbs_prim_AUDIL$esta) &
                                          tbs_prim_AUDIL$esta != "1",] %>%
            filter(!h) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Existen Audiencias sin
              Fecha de Efectivización cargada. Causas involucradas: "
          result$addError("Fecha Efectivización", paste0(mensaje, erroneas))
        }

        ambas <- !is.na(tbs_prim_AUDIL$ffa) &
                 !is.na(tbs_prim_AUDIL$fea)
        j <- tbs_prim_AUDIL[ambas,ffa] <= tbs_prim_AUDIL[ambas,fea]
        if (checkWarn(all(j))){
          e_vector <- tbs_prim_AUDIL[ambas,] %>%
            filter(!j) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Existen Causas con Fecha de Fijación mayor a la
                Fecha de Efectivización. Causas involucradas: "
          result$addError("Fecha de Fijación mayor", paste0(mensaje, erroneas))
        }

        x <- c(a, b, c, d, e, f, g, h, j)
        calidad_primaria <- round(sum(x, na.rm = T)/length(x), 4)

        # Preparar tbs primarias: Asignar clase a columnas fechas, Agregar columna de
        # mes de dictado de resolución. Convertir a mayúsculas as
          tbs_prim_AUDIL$finicio <-
            as.Date(tbs_prim_AUDIL$finicio, format="%d/%m/%Y")
          tbs_prim_AUDIL$ffa <-
            as.Date(tbs_prim_AUDIL$ffa, format="%d/%m/%Y")
          tbs_prim_AUDIL$mes_ffa <-
            month(tbs_prim_AUDIL$ffa)
          tbs_prim_AUDIL$mes_fea <-
            month(tbs_prim_AUDIL$fea)
          tbs_prim_AUDIL$ta <-
            toupper(tbs_prim_AUDIL$ta)
          tbs_prim_AUDIL$ta <-
            substr(tbs_prim_AUDIL$ta, 1, 1)
          tbs_prim_AUDIL$ra <-
            toupper(tbs_prim_AUDIL$ra)
          tbs_prim_AUDIL$mat <-
            toupper(tbs_prim_AUDIL$mat)
          tbs_prim_AUDIL$duracm <-
            as.integer(gsub("[^0-9]", "", tbs_prim_AUDIL$duracm))
          tbs_prim_AUDIL$duracb <-
            as.integer(gsub("[^0-9]", "", tbs_prim_AUDIL$duracb))

        # Creación de funciones

        p_aud_fijadas_total <- function() {
          # Cuenta audiencias informadas (total)
          aud_fijadas_total <- nrow(tbs_prim_AUDIL)
          aud_fijadas_total
        }

        p_aud_fijadas <- function() {
          # Cuenta audiencias fijadas en el mes
          aud_fijadas <- sum(self$withinInterval(tbs_prim_AUDIL$ffa), na.rm = T)
          aud_fijadas

        }

        p_aud_realizadas <- function() {
          # Cuenta audiencias realizadas en el mes
          aud_realizadas <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                                     tbs_prim_AUDIL$esta == 2, na.rm = T)
          aud_realizadas

        }

        p_aud_norealizadas <- function() {
          # Cuenta audiencias no realizadas en el mes
          aud_norealizadas <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                                      tbs_prim_AUDIL$esta == 3, na.rm = T)
          aud_norealizadas

        }

        p_aud_canceladas <- function() {
          # Cuenta audiencias canceladas en el mes
          aud_canceladas <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                                      tbs_prim_AUDIL$esta == 4, na.rm = T)
          aud_canceladas

        }

        p_aud_reprogramadas <- function() {
          # Cuenta audiencias reprogramadas en el mes
          aud_reprogramadas <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                                      tbs_prim_AUDIL$esta == 5, na.rm = T)
          aud_reprogramadas

        }



        # Funciones por Tipo de Audiencia

        p_aud_conciliacion <- function() {
          # Cuenta audiencias Conciliación realizadas en el mes
          aud_conciliacion <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                                        tbs_prim_AUDIL$esta == 2 &
                                        tbs_prim_AUDIL$ta == 'P', na.rm = T)
          aud_conciliacion
        }

        p_aud_vista_causa <- function() {
          # Cuenta audiencias Vista de Causa realizadas en el mes
          aud_vista_causa <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                                      tbs_prim_AUDIL$esta == 2 &
                                      tbs_prim_AUDIL$ta == 'V', na.rm = T)
          aud_vista_causa
        }

        p_aud_otras <- function() {
          # Cuenta audiencias Otras realizadas en el mes
          aud_otras <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                                      tbs_prim_AUDIL$esta == 2 &
                                      tbs_prim_AUDIL$ta == 'O', na.rm = T)
          aud_otras
        }


        p_aud_conct <- function() {
          # Cuenta audiencias con conciliación total
          aud_conct <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                                 tbs_prim_AUDIL$ra == "T", na.rm = T)
          aud_conct

        }

        p_aud_concp <- function() {
          # Cuenta audiencias con conciliación parcial
          aud_concp <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                                 tbs_prim_AUDIL$ra == "P", na.rm = T)
          aud_concp

        }

        p_aud_sconc <- function() {
          # Cuenta audiencias sin conciliacion
          aud_sconc <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                                 tbs_prim_AUDIL$ra == "S", na.rm = T)
          aud_sconc

        }

        p_prom_durac_ffa_frealizada <- function() {
          # Duración desde Fecha de Fijación a F.Realización
          indice_aud_realizadas <- self$withinInterval(tbs_prim_AUDIL$fea) &
            tbs_prim_AUDIL$esta == 2
          durac_ffa_frealizada <-
            mean(as.integer(tbs_prim_AUDIL$fea[indice_aud_realizadas] -
                              tbs_prim_AUDIL$ffa[indice_aud_realizadas]),
                 na.rm = T)
          durac_ffa_frealizada
        }

        p_prom_asist <- function() {
          # Promedio de asistentes por audiencia
          indice_aud_realizadas <- self$withinInterval(tbs_prim_AUDIL$fea) &
            tbs_prim_AUDIL$esta == 2
          prom_asist <- mean(tbs_prim_AUDIL$asist[indice_aud_realizadas],
                 na.rm = T)
          prom_asist
        }

        p_prom_duracm <- function() {
          # Promedio de duración en minutos por audiencia
          indice_aud_realizadas <- self$withinInterval(tbs_prim_AUDIL$fea) &
            tbs_prim_AUDIL$esta == 2
          prom_duracm <- mean(tbs_prim_AUDIL$duracm[indice_aud_realizadas],
                            na.rm = T)
          prom_duracm
        }

        p_mediana_duracm <- function() {
          # Promedio de duración en minutos por audiencia
          indice_aud_realizadas <- self$withinInterval(tbs_prim_AUDIL$fea) &
            tbs_prim_AUDIL$esta == 2
          mediana_duracm <- median(tbs_prim_AUDIL$duracm[indice_aud_realizadas],
                              na.rm = T)
          mediana_duracm
        }

        p_prom_duracb <- function() {
          # Promedio de duración en minutos por audiencia
          indice_aud_realizadas <- self$withinInterval(tbs_prim_AUDIL$fea) &
            tbs_prim_AUDIL$esta == 2
          prom_duracb <- mean(tbs_prim_AUDIL$duracb[indice_aud_realizadas],
                              na.rm = T)
          prom_duracb
        }

        p_aud_matfam <- function() {
          # Cuenta audiencias reprogramadas en el mes
          aud_matfam <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                              tbs_prim_AUDIL$esta == 2 &
                                 tbs_prim_AUDIL$mat == "F", na.rm = T)
          aud_matfam
        }

        p_aud_matciv <- function() {
          # Cuenta audiencias reprogramadas en el mes
          aud_matciv <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                              tbs_prim_AUDIL$esta == 2 &
                              tbs_prim_AUDIL$mat == "C", na.rm = T)
          aud_matciv
        }

        p_aud_matlab <- function() {
          # Cuenta audiencias reprogramadas en el mes
          aud_matlab <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                              tbs_prim_AUDIL$esta == 2 &
                              tbs_prim_AUDIL$mat == "L", na.rm = T)
          aud_matlab
        }

        p_aud_matpen <- function() {
          # Cuenta audiencias reprogramadas en el mes
          aud_matpen <- sum(self$withinInterval(tbs_prim_AUDIL$fea) &
                              tbs_prim_AUDIL$esta == 2 &
                              tbs_prim_AUDIL$mat == "P", na.rm = T)
          aud_matpen
        }

        # DESARROLLO:
        # Audiencias y tipo de proceso!

        # crear vectores
        aud_fijadas_total <- vector()
        aud_fijadas <- vector()
        aud_realizadas <- vector()
        aud_norealizadas <- vector()
        aud_canceladas <- vector()
        aud_reprogramadas <- vector()
        aud_conciliacion <- vector()
        aud_vista_causa <- vector()
        aud_otras <- vector()
        aud_conct <- vector()
        aud_concp <- vector()
        aud_sconc <- vector()
        durac_ffa_frealizada <- vector()
        prom_asist <- vector()
        prom_duracm <- vector()
        mediana_duracm <- vector()
        prom_duracb <- vector()
        aud_matfam <- vector()
        aud_matciv <- vector()
        aud_matlab <- vector()
        aud_matpen <- vector()


        # Extracción de indicadores estadísticos.Con "try" para devolver NA ante
        # falla
        aud_fijadas_total <- try(p_aud_fijadas_total(), silent = TRUE)
        aud_fijadas_total
        aud_fijadas <- try(p_aud_fijadas(), silent = TRUE)
        aud_fijadas
        aud_realizadas <- try(p_aud_realizadas(), silent = TRUE)
        aud_realizadas
        aud_norealizadas <- try(p_aud_norealizadas(), silent = TRUE)
        aud_norealizadas
        aud_canceladas <- try(p_aud_canceladas(), silent = TRUE)
        aud_canceladas
        aud_reprogramadas <- try(p_aud_reprogramadas(), silent = TRUE)
        aud_reprogramadas
        aud_conciliacion <- try(p_aud_conciliacion(), silent = TRUE)
        aud_conciliacion
        aud_vista_causa <- try(p_aud_vista_causa(), silent = TRUE)
        aud_vista_causa
        aud_otras <- try(p_aud_otras(), silent = TRUE)
        aud_otras
        aud_conct <- try(p_aud_conct(), silent = TRUE)
        aud_conct
        aud_concp <- try(p_aud_concp(), silent = TRUE)
        aud_concp
        aud_sconc <- try(p_aud_sconc(), silent = TRUE)
        aud_sconc
        durac_ffa_frealizada <- try(p_prom_durac_ffa_frealizada(), silent = TRUE)
        durac_ffa_frealizada
        prom_asist <- try(p_prom_asist(), silent = TRUE)
        prom_asist
        prom_duracm <- try(p_prom_duracm(), silent = TRUE)
        prom_duracm
        mediana_duracm <- try(p_mediana_duracm(), silent = TRUE)
        mediana_duracm
        prom_duracb <- try(p_prom_duracb(), silent = TRUE)
        prom_duracb
        aud_matfam <- try(p_aud_matfam(), silent = TRUE)
        aud_matfam
        aud_matciv <- try(p_aud_matciv(), silent = TRUE)
        aud_matciv
        aud_matlab <- try(p_aud_matlab(), silent = TRUE)
        aud_matlab
        aud_matpen <- try(p_aud_matpen(), silent = TRUE)
        aud_matpen

        # Arma df indicadores procesados
        df <- data.frame(
          aud_fijadas_total,
          aud_fijadas,
          aud_realizadas,
          aud_norealizadas,
          aud_canceladas,
          aud_reprogramadas,
          aud_conciliacion,
          aud_vista_causa,
          aud_otras,
          aud_conct,
          aud_concp,
          aud_sconc,
          durac_ffa_frealizada,
          prom_asist,
          prom_duracm,
          mediana_duracm,
          prom_duracb,
          aud_matfam,
          aud_matciv,
          aud_matlab,
          aud_matpen,
          calidad_primaria)

        # Evalúa calidad del informe presentado en base a consistencia de indicadores
        # TODO: CONSULTAR A SEBA POR VALIDACIONES DE CALIDAD EN AUDIENCIAS
         regla1 <- aud_fijadas == (aud_realizadas
                                       + aud_norealizadas)

         regla2 <- aud_norealizadas == (aud_canceladas
                                            + aud_reprogramadas)

         df$calidad <- round(sum(regla1, regla2, na.rm = T)/2, 2)

         result$addResult(df)

         result
       }

       ))






