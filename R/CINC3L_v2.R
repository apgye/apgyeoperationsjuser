# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 23-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @import lubridate
#' @importFrom stringr str_c
#' @export
CINC3L_v2 <- R6::R6Class("CINC3L_v2",
                  inherit = Operation,
                  private = list(
                    processInput = function(tbs_prim_CINC3L_v2,
                                        ril_iniciados, ril_la_iniciados,
                                        honorarios_iniciados, quejas_iniciados,
                                        rex_iniciados, otros_iniciados) {

                      colnames(tbs_prim_CINC3L_v2) <- tolower(colnames(tbs_prim_CINC3L_v2))

                      result <- OperationResult$new()
                      if (nrow(tbs_prim_CINC3L_v2) == 0){
                        df <- data.frame()
                        attr(df, "empty") <- TRUE
                        result$addResult(df)
                        return(result)
                      }

                      # Asigamos 1 a la Primaria (anteriormente ya se validó la cantidad
                      # de columnas del archivo)
                      calidad_primaria <- 1

                      ril_iniciados_tot <- sum(ril_iniciados, na.rm=T)
                      ril_la_iniciados_tot <- sum(ril_la_iniciados, na.rm=T)
                      honorarios_iniciados_tot  <- sum(honorarios_iniciados, na.rm=T)
                      quejas_iniciados_tot  <- sum(quejas_iniciados, na.rm=T)
                      rex_iniciados_tot <- sum(rex_iniciados, na.rm=T)
                      otros_iniciados_tot  <- sum(otros_iniciados, na.rm=T)

                      iniciados_totales <- tbs_prim_CINC3L_v2 %>%
                        mutate(finicio = lubridate::dmy(finicio)) %>%
                        filter(self$withinInterval(finicio)) %>%
                        nrow()

                      df <- data.frame(
                        calidad_primaria = calidad_primaria,
                        iniciados = iniciados_totales,
                        ril_iniciados = ril_iniciados_tot,
                        ril_la_iniciados = ril_la_iniciados_tot,
                        honorarios_iniciados  = honorarios_iniciados_tot,
                        quejas_iniciados  = quejas_iniciados_tot,
                        rex_iniciados = rex_iniciados_tot,
                        otros_iniciados  = otros_iniciados_tot
                       )

                      result$addResult(df)
                      result
                    }
                  )

)

