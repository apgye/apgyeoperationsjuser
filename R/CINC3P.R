# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 23-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @import lubridate
#' @importFrom stringr str_c
#' @export
CINC3P <- R6::R6Class("CINC3P",
                  inherit = Operation,
                  private = list(
                    processInput = function(tbs_prim_CINC3P) {

                      colnames(tbs_prim_CINC3P) <- tolower(colnames(tbs_prim_CINC3P))

                      result <- OperationResult$new()
                      if (nrow(tbs_prim_CINC3P) == 0){
                        df <- data.frame()
                        attr(df, "empty") <- TRUE
                        result$addResult(df)
                        return(result)
                      }

                      calidad_primaria <- 1

                      iniciados_totales <- tbs_prim_CINC3L %>%
                        mutate(finicio = lubridate::dmy(finicio)) %>%
                        filter(self$withinInterval(finicio)) %>%
                        nrow()

                      df <- data.frame(
                        calidad_primaria = calidad_primaria,
                        iniciados = iniciados_totales
                       )

                      result$addResult(df)
                      result
                    }
                  )

)

