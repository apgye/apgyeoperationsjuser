# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 24-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero
# Old-Authors: Ing. Federico Caballaro, Lic. Paolo Orundés

## input: DataFrame to Process
## output: BD_AVC_SAT_US actualization
## periodicity: On Demand
#' @export
AVC_SAT_US <- R6::R6Class("AVC_SAT_US",
       inherit = Operation,
       private = list(
       processInput = function(tbs_prim_AVC_SAT_US){

        colnames(tbs_prim_AVC_SAT_US) <- try(c("nro", "caratula", "tproc",
                                             "realizada",	"trato",	"compresion",
                                             "escucha", "duracion", "instalaciones"))

        result <- OperationResult$new()
        if (nrow(tbs_prim_AVC_SAT_US) == 0){
          df <- data.frame()
          attr(df, "empty") <- TRUE
          result$addResult(df)
          return(result)
        }

        tbs_prim_AVC_SAT_US$realizada <- as.Date(tbs_prim_AVC_SAT_US$realizada, format="%d/%m/%Y")
        inInterval <- !is.na(tbs_prim_AVC_SAT_US$realizada) &
          self$withinInterval(tbs_prim_AVC_SAT_US$realizada)

        calidad_primaria <- vector()

        aa <- tbs_prim_AVC_SAT_US %>%
          filter(!is.na(realizada)) %>% .$realizada %>% self$withinInterval()
        if (checkWarn(all(aa))){
          e_vector <- tbs_prim_AVC_SAT_US %>%
            filter(!is.na(realizada)) %>%
            filter(!aa) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "La Fecha de realización debe estar comprendida
          en el período informado. Causas involucradas: "
          result$addError("Fecha de realización", paste0(mensaje, erroneas))
        }

        a <- tbs_prim_AVC_SAT_US %>%
          .$trato %in% c(0:4)
        if (checkWarn(all(a))){
          e_vector <- tbs_prim_AVC_SAT_US %>%
            filter(!a) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El valor de Trato debe estar comprendido
          entre 0 y 4. Causas involucradas: "
          result$addError("Trato", paste0(mensaje, erroneas))
        }

        b <- tbs_prim_AVC_SAT_US %>%
          .$compresion %in% c(0:4)
        if (checkWarn(all(b))){
          e_vector <- tbs_prim_AVC_SAT_US %>%
            filter(!b) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El valor de Compresion debe estar comprendido
          entre 0 y 4. Causas involucradas: "
          result$addError("Compresion", paste0(mensaje, erroneas))
        }

        c <- tbs_prim_AVC_SAT_US %>%
          .$escucha %in% c(0:4)
        if (checkWarn(all(c))){
          e_vector <- tbs_prim_AVC_SAT_US %>%
            filter(!c) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El valor de Escucha debe estar comprendido
          entre 0 y 4. Causas involucradas: "
          result$addError("Escucha", paste0(mensaje, erroneas))
        }

        cc <- tbs_prim_AVC_SAT_US %>%
          .$duracion %in% c(0:4)
        if (checkWarn(all(cc))){
          e_vector <- tbs_prim_AVC_SAT_US %>%
            filter(!cc) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El valor de Duración debe estar comprendido
          entre 0 y 4. Causas involucradas: "
          result$addError("Duración", paste0(mensaje, erroneas))
        }

        d <- tbs_prim_AVC_SAT_US %>%
          .$instalaciones %in% c(0:4)
        if (checkWarn(all(d))){
          e_vector <- tbs_prim_AVC_SAT_US %>%
            filter(!d) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El valor de Instalaciones debe estar comprendido
          entre 0 y 4. Causas involucradas: "
          result$addError("Instalaciones", paste0(mensaje, erroneas))
        }

        # Validacion Fecha
        e <- try(all(!is.null(tbs_prim_AVC_SAT_US$realizada) |
                       !is.na(tbs_prim_AVC_SAT_US$realizada)))
        if (checkWarn(e)){
          result$addError("Fecha Realización", "Existen encuestas sin Fecha
                          de realización de audiencia.")
        }

        x <- c(aa, a, b, c, cc, d, e)
        calidad_primaria <- round(sum(x, na.rm = T)/length(x), 4)

        # Preparar tbs primarias: Asignar clase a columnas fechas, Agregar columna de
        # mes de dictado de resolución. Convertir a mayúsculas as
        tbs_prim_AVC_SAT_US$realizada <-
          as.Date(tbs_prim_AVC_SAT_US$realizada, format="%d/%m/%Y")
        tbs_prim_AVC_SAT_US$trato <-
          as.integer(tbs_prim_AVC_SAT_US$trato)
        tbs_prim_AVC_SAT_US$compresion <-
          as.integer(tbs_prim_AVC_SAT_US$compresion)
        tbs_prim_AVC_SAT_US$escucha <-
          as.integer(tbs_prim_AVC_SAT_US$escucha)
        tbs_prim_AVC_SAT_US$instalaciones <-
          as.integer(tbs_prim_AVC_SAT_US$instalaciones)


        # Procesar
        df <- tbs_prim_AVC_SAT_US %>%
          filter(inInterval) %>%
          mutate(trato = ifelse(trato == 0, NA, trato),
                 escucha = ifelse(escucha == 0, NA, escucha),
                 compresion = ifelse(compresion == 0, NA, compresion),
                 duracion = ifelse(duracion == 0, NA, duracion),
                 instalaciones = ifelse(instalaciones == 0, NA, instalaciones)) %>%
          summarise_if(is.numeric, mean, na.rm=TRUE) %>%
          mutate(calidad_primaria = calidad_primaria)

        result$addResult(df)

        result
        }

       ))
