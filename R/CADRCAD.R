# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 26-06-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @import dplyr
#' @import apgyeCommon
#' @export
CADRCAD <- R6::R6Class("CADRCAD",
              inherit = Operation,
              private = list(
                processInput = function( tbs_prim_CADRCAD ) {

                  result <- OperationResult$new()
                  if (nrow(tbs_prim_CADRCAD) == 0){
                    df <- data.frame()
                    attr(df, "empty") <- TRUE
                    result$addResult(df)

                    return(result)
                  }

              # "expectedColumns": ["nro", "caratula", "tproc", "finicio",
              #                     "faperap", "fadmis", "fdesp", "fvenc",
              #                     "fres", "as", "tres", "tdem",
              #                     "odem", "tdemo", "tmca", "ordv",
              #                     "rec"],
                  calidad_primaria <- vector()

                  tbs_prim_CADRCAD$fres <-
                    as.Date(tbs_prim_CADRCAD$fres, format="%d/%m/%Y")

                  res_inInterval <- !is.na(tbs_prim_CADRCAD$fres) &
                    self$withinInterval(tbs_prim_CADRCAD$fres)

                  a <- tbs_prim_CADRCAD$as %in% c("A", "S", "a", "s")
                  if (checkWarn(all(a))){
                    e_vector <- tbs_prim_CADRCAD %>%
                      filter(!a) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "Existen Causas sin declarar
                            A (Auto) ó S (Sentencia) ó declarando incorrectamente
                            según codificación. Causas involucradas: "
                    result$addError("Auto o Sentencia", paste0(mensaje, erroneas))
                  }
                  b <- tbs_prim_CADRCAD %>%
                    filter(!is.na(fres)) %>% .$fres %>% self$withinInterval()
                  if (checkWarn(all(b))){
                    e_vector <- tbs_prim_CADRCAD[!is.na(tbs_prim_CADRCAD$fres),] %>%
                      filter(!b) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "La Fecha de Resolución debe
                            estar comprendida en el período informado. Causas involucradas: "
                    result$addError("Fecha de Resolución", paste0(mensaje, erroneas))
                  }
                  c <- tbs_prim_CADRCAD[res_inInterval, tres] %in% c(0:12)
                  if (checkWarn(all(c))){
                    e_vector <- tbs_prim_CADRCAD[res_inInterval] %>%
                      filter(!c) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "Para las Causas Resueltas en el período
                    informado, el Tipo de Resolución debe
                    estar comprendido entre 0 y 12. Causas involucradas: "
                    result$addError("Tipo de Resolución", paste0(mensaje, erroneas))
                  }
                  d <- tbs_prim_CADRCAD$tdem %in% c(1:19, NA)
                  if (checkWarn(all(d))){
                    e_vector <- tbs_prim_CADRCAD %>%
                      filter(!d) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "El Tipo Proceso (Demanda)
                      debe estar comprendido entre 1 y 14 y para los Procesos en
                      Apelación Ley 10636 debe estar comprendido entre 15 y 19.
                      Causas involucradas: "
                    result$addError("Tipo de Proceso (Demanda)", paste0(mensaje, erroneas))
                  }
                  e <- tbs_prim_CADRCAD$odem %in% c(1:11, NA)
                  if (checkWarn(all(e))){
                    e_vector <- tbs_prim_CADRCAD %>%
                      filter(!e) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "El Objeto de Demanda debe estar comprendido
                      entre 1 y 11. Causas involucradas: "
                    result$addError("Objeto de Demanda", paste0(mensaje, erroneas))
                  }
                  f <- tbs_prim_CADRCAD$tdemo %in% c(1:5, NA)
                  if (checkWarn(all(f))){
                    e_vector <- tbs_prim_CADRCAD %>%
                      filter(!f) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "El Tipo de Demandado debe estar comprendido
                      entre 1 y 5. Causas involucradas: "
                    result$addError("Tipo de Demandado", paste0(mensaje, erroneas))
                  }
                  g <- tbs_prim_CADRCAD$tmca %in% c(1:7, NA)
                  if (checkWarn(all(g))){
                    e_vector <- tbs_prim_CADRCAD %>%
                      filter(!g) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "El Tipo de Medidas Cautelares debe estar
                      comprendido entre 1 y 7. Causas involucradas: "
                    result$addError("Tipo de Medidas Cautelares", paste0(mensaje, erroneas))
                  }
                  h <- tbs_prim_CADRCAD$rec %in% c(1:10, NA)
                  if (checkWarn(all(h))){
                    e_vector <- tbs_prim_CADRCAD %>%
                      filter(!h) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "La Resolución según tipo de recurso debe estar
                      comprendido entre 1 y 10. Causas involucradas: "
                    result$addError("Resolución según tipo de recurso", paste0(mensaje, erroneas))
                  }

                  ordvs <- stringr::str_trim(unlist(stringr::str_split(
                    tbs_prim_CADRCAD[res_inInterval, ordv], "[:punct:]")))
                  j <- ordvs %in% magist_func_id_agentes
                  if (checkWarn(all(j))){
                    tf_vector <- purrr::map_lgl(tbs_prim_CADRCAD[res_inInterval, ordv], function(item){
                      x <- stringr::str_trim(unlist(stringr::str_split(
                        item, "[:punct:]")))
                      res <- all(x %in% magist_func_id_agentes)
                      res
                    })
                    e_vector <- tbs_prim_CADRCAD[res_inInterval, ] %>%
                      filter(!tf_vector) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "Para las Causas Resueltas en el período informado,
                            existe/n código/s declarados para el Magistrado
                            no reconocidos o faltantes. Causas involucradas: "
                    result$addError("Orden de Votación -ordv-", paste0(mensaje, erroneas))
                  }

                  # Date Formats

                  tbs_prim_CADRCAD$finicio <- as.Date(tbs_prim_CADRCAD$finicio,
                                                       format="%d/%m/%Y")
                  tbs_prim_CADRCAD$fdesp <- as.Date(tbs_prim_CADRCAD$fdesp,
                                                    format="%d/%m/%Y")
                  tbs_prim_CADRCAD$fvenc <- as.Date(tbs_prim_CADRCAD$fvenc,
                                                    format="%d/%m/%Y")
                  tbs_prim_CADRCAD$fres <- as.Date(tbs_prim_CADRCAD$fres,
                                                    format="%d/%m/%Y")
                  # Date Validations
                  ambas1 <- !is.na(tbs_prim_CADRCAD$finicio) &
                    !is.na(tbs_prim_CADRCAD$fdesp)
                  k <- tbs_prim_CADRCAD[ambas1, finicio] <= tbs_prim_CADRCAD[ambas1, fdesp]
                  if (checkWarn(all(k))){
                    e_vector <- tbs_prim_CADRCAD[ambas1] %>%
                      filter(!k) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "Existen Causas con Fecha de Despacho anterior
                            a la Fecha de Inicio. Causas involucradas: "
                    result$addError("Fecha de Despacho", paste0(mensaje, erroneas))
                  }

                  ambas2 <- !is.na(tbs_prim_CADRCAD$fdesp) &
                    !is.na(tbs_prim_CADRCAD$fvenc)
                  l <- tbs_prim_CADRCAD[ambas2, fdesp] <= tbs_prim_CADRCAD[ambas2, fvenc]
                  if (checkWarn(all(l))){
                    e_vector <- tbs_prim_CADRCAD[ambas2] %>%
                      filter(!l) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "Existen Causas con Fecha de Vencimiento anterior
                            a la Fecha de Despacho. Causas involucradas: "
                    result$addError("Fecha de Vencimiento", paste0(mensaje, erroneas))
                  }

                  ambas3 <- !is.na(tbs_prim_CADRCAD$fdesp) &
                    !is.na(tbs_prim_CADRCAD$fres)
                  m <- tbs_prim_CADRCAD[ambas3, fdesp] <= tbs_prim_CADRCAD[ambas3, fres]
                  if (checkWarn(all(m))){
                    e_vector <- tbs_prim_CADRCAD[ambas3] %>%
                      filter(!m) %>% .$nro
                    erroneas <- paste0(e_vector, collapse = ", ")
                    mensaje <- "Existen Causas con Fecha de Resolución anterior
                            a la Fecha de Despacho. Causas involucradas: "
                    result$addError("Fecha de Resolución anterior", paste0(mensaje, erroneas))
                  }

                  x <- c(a, b, c, d, e, f, g, h, j, k, l, m)
                  calidad_primaria <- round(sum(x, na.rm = T)/length(x), 4)

                  # Some fields format
                  tbs_prim_CADRCAD$faperap <- as.Date(tbs_prim_CADRCAD$faperap,
                                                     format="%d/%m/%Y")
                  tbs_prim_CADRCAD$fadmis <- as.Date(tbs_prim_CADRCAD$fadmis,
                                                      format="%d/%m/%Y")
                  tbs_prim_CADRCAD$mes_res <- month(tbs_prim_CADRCAD$fres)
                  tbs_prim_CADRCAD$as <- toupper(tbs_prim_CADRCAD$as)

                  # Medidas mejor proveer
                  no_mmp <- tbs_prim_CADRCAD$tres != 0 | is.na(tbs_prim_CADRCAD$tres)

                  # Functions creations
                  p_causasadespacho <- function() {
                    # Suma causas con fdesp en el mes que se informa sin los NA
                    causas_adespacho <- sum(self$withinInterval(tbs_prim_CADRCAD$fdesp), na.rm = TRUE)
                    causas_adespacho
                  }

                  p_causasresueltas <- function() {
                    # Suma causas con resolución en el mes que se informa que no sean medidas
                    # para mejor proveer
                    causas_resueltas <- sum(self$withinInterval(tbs_prim_CADRCAD$fres) &
                                              no_mmp, na.rm = T)
                    causas_resueltas
                  }

                  p_resxsentencia <- function() {
                    # Suma sentencias
                    res_xsentencia <- sum(self$withinInterval(tbs_prim_CADRCAD$fres) &
                                            tbs_prim_CADRCAD$as == "S" &
                                            no_mmp, na.rm = T)
                    res_xsentencia
                  }

                  p_resxauto <- function() {
                    # Suma autos
                    res_xauto <- sum(self$withinInterval(tbs_prim_CADRCAD$fres) &
                                       tbs_prim_CADRCAD$as == "A" & no_mmp, na.rm = T)
                    res_xauto
                  }

                  p_resxotra <- function() {
                    # Suma resoluciones otraminadas
                    res_xotra <- sum(self$withinInterval(tbs_prim_CADRCAD$fres) &
                                       !tbs_prim_CADRCAD$as %in% c("A", "S") &
                                       no_mmp, na.rm = T)
                    res_xotra
                  }

                  p_res_atermino <- function() {
                    # Suma resoluciones con fecha menor o igual a la fecha de 1er. venc que
                    # no sean medidas para mejor proveer
                    res_atermino <- sum(tbs_prim_CADRCAD$fres <=
                                          tbs_prim_CADRCAD$fvenc &
                                          self$withinInterval(tbs_prim_CADRCAD$fres) &
                                          no_mmp, na.rm = T)
                    res_atermino
                  }

                  p_res_luegovenc <- function() {
                    # Suma resoluciones con fecha mayor a la del vencimiento
                    # sin medidas para mejor proveer
                    res_luegovenc <- sum(tbs_prim_CADRCAD$fres >
                                            tbs_prim_CADRCAD$fvenc &
                                            self$withinInterval(tbs_prim_CADRCAD$fres) &
                                            no_mmp, na.rm = T)
                    res_luegovenc
                  }


                  p_durac_inic_sent_xproc <- function() {
                    # Duración de Procesos desde Fecha de Inicio hasta Sentencia en días
                    # corridos.
                    # ADVERTENCIA: dada esta medición quedan excluidos del indicador de
                    # duración los procesos sucesorios y ejecutivos
                    indice_sent <- tbs_prim_CADRCAD$as == "S" & res_inInterval
                    # Vector tipo de proceso
                    vtproc <- tbs_prim_CADRCAD$tproc[indice_sent]
                    # Vector duracion
                    vdurac <- tbs_prim_CADRCAD$fres[indice_sent] - tbs_prim_CADRCAD$finicio[indice_sent]
                    # Vector fres
                    vfres <- tbs_prim_CADRCAD$fres[indice_sent]
                    # Vector nro
                    vnro <- tbs_prim_CADRCAD$nro[indice_sent]
                    # Vecto combinado
                    vcomb <- str_c(vtproc, vdurac, vfres, vnro, sep = "&")
                    vcomb <- noquote(vcomb[!is.na(vcomb)])
                    durac_inic_sent_xproc <- str_c(vcomb, collapse = "#")
                    if (purrr::is_empty(durac_inic_sent_xproc)) {
                      durac_inic_sent_xproc <- NA
                    } else {
                      durac_inic_sent_xproc
                    }
                    durac_inic_sent_xproc
                  }


                  # Vectors creations
                  causas_adespacho <- vector()
                  causas_resueltas <- vector()
                  res_xsentencia <- vector()
                  res_xauto <- vector()
                  res_xotra <- vector()
                  res_atermino <- vector()
                  res_luegovenc <- vector()
                  durac_inic_sent_xproc <- vector()

                  # Extracción de indicadores estadísticos. Con "try" para devolver NA ante
                  # falla

                  causas_adespacho <- try(p_causasadespacho(), silent = TRUE)
                  causas_adespacho
                  causas_resueltas <- try(p_causasresueltas(), silent = TRUE)
                  causas_resueltas
                  res_xsentencia <- try(p_resxsentencia(), silent = TRUE)
                  res_xsentencia
                  res_xauto <- try(p_resxauto(), silent = TRUE)
                  res_xauto
                  res_xotra <- try(p_resxotra(), silent = TRUE)
                  res_xotra
                  res_atermino <- try(p_res_atermino(), silent = TRUE)
                  res_atermino
                  res_luegovenc <- try(p_res_luegovenc(), silent = TRUE)
                  res_luegovenc
                  durac_inic_sent_xproc <- try(p_durac_inic_sent_xproc(), silent = TRUE)
                  durac_inic_sent_xproc



                  # Arma df indicadores procesados
                  df <- data.frame(
                    causas_adespacho,
                    causas_resueltas,
                    res_xsentencia,
                    res_xauto,
                    res_xotra,
                    res_atermino,
                    res_luegovenc,
                    durac_inic_sent_xproc,
                    calidad_primaria)


                  # Evalúa calidad del informe presentado en base a consistencia de indicadores
                  # 1: Causas resueltas == resueltas x S + x A + x otra
                  # 2: Causas resoluestas == resueltas a termino + luego1v + luego2v
                  #     Como en estas columnas se presentan NA a veces no se puede evaluar la
                  #     la función de procesamiento
                  # 3: 0 < Causas Iniciadas (ppales + no ppales) + causas archivadas

                  regla1 <- df$causas_resueltas == (df$res_xsentencia + df$res_xauto +
                                                      df$res_xotra)
                  regla2 <- df$causas_resueltas == (df$res_atermino + df$res_luegovenc)
                  df$calidad <- round(sum(regla1 + regla2, na.rm = T)/2, 2)


                  # sentencias_pendientes <- tbs_prim_CADRCAD %>%
                  #   filter(is.na(fres), as =="S") %>%
                  #   mutate(sentencias_pendientes_vencidas = fvenc < lubridate::int_end(self$interval)) %>%
                  #   arrange(fdesp) %>%
                  #   dplyr::summarise(sentencias_pendientes = n(), sentencias_pendientes_vencidas = sum(sentencias_pendientes_vencidas), s_fdespacho_mas_antigua=fdesp[1], s_catatula_mas_antigua=caratula[1])
                  #
                  # autos_pendientes <- tbs_prim_CADRCAD %>%
                  #   filter(is.na(fres), as =="A") %>%
                  #   mutate(autos_pendientes_vencidos = fvenc < lubridate::int_end(self$interval)) %>%
                  #   arrange(fdesp) %>%
                  #   dplyr::summarise(autos_pendientes = n(), autos_pendientes_vencidos = sum(autos_pendientes_vencidos), a_fdespacho_mas_antigua=fdesp[1], a_catatula_mas_antigua=caratula[1])
                  #
                  # listado_pendientes <- tbs_prim_CADRCAD %>%
                  #   filter(is.na(fres)) %>%
                  #   mutate(vencido = fvenc < lubridate::int_end(self$interval)) %>%
                  #   arrange(fdesp)
                  #
                  # pendientes <- cbind(sentencias_pendientes, autos_pendientes)
                  # pendientes$total_adespacho <- causas_adespacho
                  # pendientes <- pendientes %>%
                  #   mutate(total_pendientes = nrow(listado_pendientes)) %>%
                  #   mutate(total_pendientes_vencidas = listado_pendientes %>% filter(vencido) %>% nrow()) %>%
                  #   mutate(porcentaje_pendientes = total_pendientes / causas_adespacho )
                  #
                  # result$addResult(pendientes, name="pendientes")
                  # result$addResult(listado_pendientes, name="listado_pendientes")

                  # Causas a despacho para el Boletin Oficial-----------------------------
                  adespacho <- data.frame(
                    caratula =  tbs_prim_CADRCAD$caratula[is.na(tbs_prim_CADRCAD$fres)],
                    fdesp = tbs_prim_CADRCAD$fdesp[is.na(tbs_prim_CADRCAD$fres)]
                  )

                  result$addResult(df)
                  result$addResult(adespacho, name="adespacho")

                  result
                  }
                  )
)
