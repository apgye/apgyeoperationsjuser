# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 23-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero
# Old-Authors: Ing. Federico Caballaro, Lic. Paolo Orundés

## input: DataFrame to Process
## output: BD_CADR1C_v2_L8 actualization
## periodicity: On Demand
#' @import lubridate
#' @importFrom stringr str_c
#' @export
CADR1C_v2_L8 <- R6::R6Class("CADR1C_v2_L8",
                    inherit = Operation,
                    private = list(
                      processInput = function(tbs_prim_CADR1C_v2_L8){
                        result <- OperationResult$new()
                        if (nrow(tbs_prim_CADR1C_v2_L8) == 0){
                          df <- data.frame()
                          attr(df, "empty") <- TRUE
                          result$addResult(df)
                          return(result)
                        }

                        colnames(tbs_prim_CADR1C_v2_L8) <- try(c("nro",
                                                              "caratula",
                                                              "tproc",
                                                              "as",#Error-a
                                                              "ccon",
                                                              "finicio",
                                                              "fdesp",#Error-d
                                                              "fvenc1",#Error-e
                                                              "fvenc2",
                                                              "fres",
                                                              "tres",#Error-c
                                                              "mat",
                                                              "justiciabl",
                                                              "reccap",
                                                              "codigo_jue"#Error-j
                                                           ))

                        calidad_primaria <- vector()
                        tbs_prim_CADR1C_v2_L8$fres <-
                          as.Date(tbs_prim_CADR1C_v2_L8$fres, format="%d/%m/%Y")

                        res_inInterval <- !is.na(tbs_prim_CADR1C_v2_L8$fres) &
                          self$withinInterval(tbs_prim_CADR1C_v2_L8$fres)

                        as_expected <- c("A", "S", "a", "s", "S1",
                                         "S2", "S3", "S4", "S5", "S6", "S7",
                                         "s1", "s2", "s3", "s4", "s5", "s6",
                                         "s7")

                        s_expected <- c("S", "s", "S1",
                                        "S2", "S3", "S4", "S5", "S6", "S7",
                                        "s1", "s2", "s3", "s4", "s5", "s6",
                                        "s7")

                        a <- tbs_prim_CADR1C_v2_L8$as %in% as_expected
                        if (checkWarn(all(a))){
                          e_vector <- tbs_prim_CADR1C_v2_L8 %>%
                            filter(!a) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Existen Causas sin declarar
                          A (Auto) ó S (Sentencia) ó declarando incorrectamente
                          según codificación. Causas involucradas: "
                          result$addError("Auto o Sentencia", paste0(mensaje, erroneas))
                        }
                        b <- tbs_prim_CADR1C_v2_L8 %>%
                          filter(!is.na(fres)) %>% .$fres %>% self$withinInterval()
                        if (checkWarn(all(b))){
                          e_vector <- tbs_prim_CADR1C_v2_L8[!is.na(tbs_prim_CADR1C_v2_L8$fres),] %>%
                            filter(!b) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "La Fecha de Resolución debe
                          estar comprendida en el período informado. Causas involucradas: "
                          result$addError("Fecha de Resolución", paste0(mensaje, erroneas))
                        }

                        c <- tbs_prim_CADR1C_v2_L8[res_inInterval &
                                                  as %in% s_expected, tres] %in% c(0:37, NA)
                        if (checkWarn(all(c))){
                          e_vector <- tbs_prim_CADR1C_v2_L8[res_inInterval &
                                                           as %in% s_expected] %>%
                            filter(!c) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Para las Sentencias Resueltas en el período
                          informado, el Tipo de Resolución debe
                          estar comprendido entre 0 y 37. Causas involucradas: "
                          result$addWarning("Tipo de Resolución", paste0(mensaje, erroneas))
                        }

                        # Dates validations
                        tbs_prim_CADR1C_v2_L8$fdesp <-
                          as.Date(tbs_prim_CADR1C_v2_L8$fdesp, format="%d/%m/%Y")
                        tbs_prim_CADR1C_v2_L8$fvenc1 <-
                          as.Date(tbs_prim_CADR1C_v2_L8$fvenc1, format="%d/%m/%Y")
                        tbs_prim_CADR1C_v2_L8$fvenc2 <-
                          as.Date(tbs_prim_CADR1C_v2_L8$fvenc2, format="%d/%m/%Y")



                        d <- !is.na(tbs_prim_CADR1C_v2_L8$fdesp)
                        if (checkWarn(all(d))){
                          e_vector <- tbs_prim_CADR1C_v2_L8 %>%
                            filter(!d) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Existen Causas sin Fecha a Despacho. Causas involucradas: "
                          result$addError("Fecha a Despacho", paste0(mensaje, erroneas))
                        }

                        e <- !is.na(tbs_prim_CADR1C_v2_L8$fvenc1)
                        if (checkWarn(all(e))){
                          e_vector <- tbs_prim_CADR1C_v2_L8 %>%
                            filter(!e) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Existen Causas sin Fecha de Vencimiento 1. Causas involucradas: "
                          result$addError("Fecha de Vencimiento 1", paste0(mensaje, erroneas))
                        }

                        ambas <- !is.na(tbs_prim_CADR1C_v2_L8$fdesp) &
                          !is.na(tbs_prim_CADR1C_v2_L8$fvenc1)
                        f <- tbs_prim_CADR1C_v2_L8[ambas, fdesp] <= tbs_prim_CADR1C_v2_L8[ambas, fvenc1]
                        if (checkWarn(all(f))){
                          e_vector <- tbs_prim_CADR1C_v2_L8[ambas] %>%
                            filter(!f) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Existen Causas con Fecha de Despacho mayor
                            a la Fecha de Vencimiento 1. Causas involucradas: "
                          result$addWarning("Fecha de Despacho", paste0(mensaje, erroneas))
                        }

                        if ((month(int_start(self$interval))+1 == month(Sys.Date()) |
                             month(int_start(self$interval)) == 12 & month(Sys.Date()) %in% c(1,2)) & #add by T2328
                            exists("id_agentes_gbl")){
                          magist_func_id_agentes <- id_agentes_gbl

                        } else {
                          # buscaremos los id_agentes para el período a presentar. caso Fam2Pna(255-Solari)
                          magist_func_id_agentes <- magistrados_y_secretarios_periodo(DB_PROD(),
                                                                                      int_start(self$interval),
                                                                                      int_end(self$interval)) %>%
                            pull(idagente)

                          # add "0" ti IDs for External Agent
                          magist_func_id_agentes[length(magist_func_id_agentes)+1] <- "0"
                        }

                        # codigo_juez VALIDATION
                        j <- tbs_prim_CADR1C_v2_L8[res_inInterval, codigo_jue] %in% magist_func_id_agentes
                        if (checkWarn(all(j))){
                          e_vector <- tbs_prim_CADR1C_v2_L8[res_inInterval] %>%
                            filter(!j) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Para las Causas Resueltas, existe/n código/s
                            declarados para el Juez no reconocidos o faltantes. Causas involucradas: "
                          result$addError("Código del Juez", paste0(mensaje, erroneas))
                        }

                        ambas <- !is.na(tbs_prim_CADR1C_v2_L8$fdesp) & !is.na(tbs_prim_CADR1C_v2_L8$fvenc2)
                        k <- tbs_prim_CADR1C_v2_L8[ambas, fdesp] <= tbs_prim_CADR1C_v2_L8[ambas, fvenc2]
                        if (checkWarn(all(k))){
                          e_vector <- tbs_prim_CADR1C_v2_L8[ambas] %>%
                            filter(!k) %>% .$nro
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Existen Causas con Fecha de Despacho mayor
                            a la Fecha de Vencimiento 2. Causas involucradas: "
                          result$addWarning("Fecha de Despacho y 2do vencimiento", paste0(mensaje, erroneas))
                        }

                        # Validamos que nro y caratula no sean vacíos - Taiga1166
                        m <- !(is.na(tbs_prim_CADR1C_v2_L8$nro) & is.na(tbs_prim_CADR1C_v2_L8$caratula))
                        if (checkWarn(all(m))){
                          e_vector <- tbs_prim_CADR1C_v2_L8 %>%
                            filter(!m) %>% .$finicio
                          erroneas <- paste0(e_vector, collapse = ", ")
                          mensaje <- "Existen Causas sin Número (nro) ni Carátula. Causas involucradas con fecha de inicio: "
                          result$addError("Número - Carátula", paste0(mensaje, erroneas))
                        }

                        x <- c(a, b, c, d, e, f, j, k, m)
                        calidad_primaria <- round(sum(x, na.rm = T)/length(x), 4)

                        # Some fields format

                        tbs_prim_CADR1C_v2_L8$finicio <- as.Date(tbs_prim_CADR1C_v2_L8$finicio,
                                                             format="%d/%m/%Y")
                        tbs_prim_CADR1C_v2_L8$fdesp <- as.Date(tbs_prim_CADR1C_v2_L8$fdesp,
                                                           format="%d/%m/%Y")
                        tbs_prim_CADR1C_v2_L8$fres <- as.Date(tbs_prim_CADR1C_v2_L8$fres,
                                                           format="%d/%m/%Y")
                        tbs_prim_CADR1C_v2_L8$fvenc1 <- as.Date(tbs_prim_CADR1C_v2_L8$fvenc1,
                                                            format="%d/%m/%Y")
                        tbs_prim_CADR1C_v2_L8$fvenc2 <- as.Date(tbs_prim_CADR1C_v2_L8$fvenc2,
                                                            format="%d/%m/%Y")
                        tbs_prim_CADR1C_v2_L8$mes_res <- month(tbs_prim_CADR1C_v2_L8$fres)
                        tbs_prim_CADR1C_v2_L8$as <- toupper(tbs_prim_CADR1C_v2_L8$as)

                        # Medidas mejor proveer
                        no_mmp <- tbs_prim_CADR1C_v2_L8$tres != 0 | is.na(tbs_prim_CADR1C_v2_L8$tres)

                        # Functions creations
                        p_causasadespacho <- function() {
                          # Suma causas con fdesp en el mes que se informa sin los NA
                          causas_adespacho <- sum(self$withinInterval(tbs_prim_CADR1C_v2_L8$fdesp), na.rm = TRUE)
                          causas_adespacho
                        }

                        p_causasresueltas <- function() {
                          # Suma causas con resolución en el mes que se informa que no sean medidas
                          # para mejor proveer
                          causas_resueltas <- sum(res_inInterval &
                                                    no_mmp, na.rm = T)
                          causas_resueltas
                        }

                        p_resxsentencia <- function() {
                          # Suma sentencias
                          res_xsentencia <- sum(res_inInterval &
                                                  tbs_prim_CADR1C_v2_L8$as %in%
                                                      c("S", "S1", "S2", "S3",
                                                      "S4", "S5", "S6", "S7") &
                                                  no_mmp, na.rm = T)
                          res_xsentencia
                        }

                        p_resxauto <- function() {
                          # Suma autos
                          res_xauto <- sum(res_inInterval &
                                             tbs_prim_CADR1C_v2_L8$as == "A" & no_mmp, na.rm = T)
                          res_xauto
                        }

                        p_resxindeter <- function() {
                          # Suma resoluciones otraminadas
                          res_xindeter <- sum(res_inInterval &
                                             !tbs_prim_CADR1C_v2_L8$as %in% c("A", "S") &
                                             no_mmp, na.rm = T)
                          res_xindeter
                        }

                        p_resccon <- function() {
                          # Suma resoluciones en causas con contención
                          res_ccon <- sum(res_inInterval &
                                            tbs_prim_CADR1C_v2_L8$ccon == 1 &
                                            no_mmp, na.rm = T)
                          res_ccon
                        }

                        p_sentmon_sinc <- function() {
                          # Suma sentencias monitorias sin contención
                          sentmon_sinc <- sum(res_inInterval &
                                                tbs_prim_CADR1C_v2_L8$tres == 8, na.rm = T)
                          sentmon_sinc
                        }

                        p_sentmon_ccon <- function() {
                          # Suma sentencias monitorias sin contención
                          sentmon_ccon <- sum(res_inInterval &
                                                tbs_prim_CADR1C_v2_L8$tres == 9, na.rm = T)
                          sentmon_ccon
                        }

                        p_res_atermino <- function() {
                          # Suma resoluciones con fecha menor o igual a la fecha de 1er. venc que
                          # no sean medidas para mejor proveer
                          res_atermino <- sum(tbs_prim_CADR1C_v2_L8$fres <=
                                                tbs_prim_CADR1C_v2_L8$fvenc1 &
                                                res_inInterval &
                                                no_mmp, na.rm = T)
                          res_atermino
                        }

                        p_res_luego1venc <- function() {
                          # Suma resoluciones con fecha mayor a la de 1er. vencimiento y menor a la
                          # del segundo sin medidas para mejor proveer
                          res_luego1venc <- sum(tbs_prim_CADR1C_v2_L8$fres >
                                                  tbs_prim_CADR1C_v2_L8$fvenc1 &
                                                  tbs_prim_CADR1C_v2_L8$fres <=
                                                  tbs_prim_CADR1C_v2_L8$fvenc2 &
                                                  res_inInterval &
                                                  no_mmp, na.rm = T)
                          res_luego1venc
                        }

                        p_res_luego2venc <- function() {
                          # Suma resoluciones con fecha mayor a la del 2do. vencimiento que no son
                          # medida para mejor proveer
                          res_luego2venc <-sum(tbs_prim_CADR1C_v2_L8$fres >
                                                 tbs_prim_CADR1C_v2_L8$fvenc2 &
                                                 res_inInterval &
                                                 no_mmp, na.rm = T)
                          res_luego2venc
                        }

                        p_prom_durac_inic_sent <- function() {
                          # Duración de Procesos desde Fecha de Inicio hasta Sentencia: PROMEDIO
                          # en días corridos
                          # ADVERTENCIA: dada esta medición quedan excluidos del indicador de
                          # duración los procesos sucesorios y ejecutivos
                          indice_sent <- tbs_prim_CADR1C_v2_L8$as == "S" &
                            res_inInterval
                          durac_inic_sent <-
                            mean(as.integer(tbs_prim_CADR1C_v2_L8$fres[indice_sent] -
                                              tbs_prim_CADR1C_v2_L8$finicio[indice_sent]),
                                 na.rm = T)
                          durac_inic_sent
                        }

                        p_durac_inic_sent_xproc <- function() {
                          # Duración de Procesos desde Fecha de Inicio hasta Sentencia en días
                          # corridos.
                          # ADVERTENCIA: dada esta medición quedan excluidos del indicador de
                          # duración los procesos sucesorios y ejecutivos
                          indice_sent <- tbs_prim_CADR1C_v2_L8$as == "S" &
                            res_inInterval
                          # Vector tipo de proceso
                          vtproc <- tbs_prim_CADR1C_v2_L8$tproc[indice_sent]
                          # Vector duracion
                          vdurac <- tbs_prim_CADR1C_v2_L8$fres[indice_sent] -
                            tbs_prim_CADR1C_v2_L8$finicio[indice_sent]
                          # Vecto combinado
                          vcomb <- str_c(vtproc, vdurac, sep = "&")
                          vcomb <- noquote(vcomb[!is.na(vcomb)])
                          durac_inic_sent_xproc <- str_c(vcomb, collapse = "#")
                          if (purrr::is_empty(durac_inic_sent_xproc)) {
                            durac_inic_sent_xproc <- NA
                          } else {
                            durac_inic_sent_xproc
                          }
                          durac_inic_sent_xproc
                        }

                        p_ccausa_ant <- function() {
                          # Causa informada con fecha de despacho más antigua: Caratula
                          if (length(na.omit(tbs_prim_CADR1C_v2_L8$fdesp)) > 0) {
                            ccausa_ant <-
                              tbs_prim_CADR1C_v2_L8[[which.min(tbs_prim_CADR1C_v2_L8$fdesp),
                                                 "caratula"]]
                            ccausa_ant
                          } else {
                            NA
                          }
                        }

                        p_fcausa_ant <- function() {
                          # Causa informada con fecha de despacho más antigua: Fecha
                          if (length(na.omit(tbs_prim_CADR1C_v2_L8$fdesp)) > 0) {
                            fcausa_ant <-
                              tbs_prim_CADR1C_v2_L8[[which.min(tbs_prim_CADR1C_v2_L8$fdesp),
                                                 "fdesp"]]
                            fcausa_ant <- as.Date(fcausa_ant)
                            fcausa_ant
                          } else {
                            NA
                          }
                        }

                        # Vectors creations
                        causas_adespacho <- vector()
                        causas_resueltas <- vector()
                        res_xsentencia <- vector()
                        res_xauto <- vector()
                        res_xindeter <- vector()
                        res_atermino <- vector()
                        res_luego1venc <- vector()
                        res_luego2venc <- vector()
                        prom_durac_inic_sent <- vector()
                        durac_inic_sent_xproc <- vector()
                        ccausa_ant <- vector()
                        fcausa_ant <- vector()
                        res_ccon <- vector()
                        sentmon_sinc <- vector()
                        sentmon_ccon <- vector()

                        # Extracción de indicadores estadísticos. Con "try" para devolver NA ante
                        # falla

                        causas_adespacho <- try(p_causasadespacho(), silent = TRUE)
                        causas_adespacho
                        causas_resueltas <- try(p_causasresueltas(), silent = TRUE)
                        causas_resueltas
                        res_xsentencia <- try(p_resxsentencia(), silent = TRUE)
                        res_xsentencia
                        res_xauto <- try(p_resxauto(), silent = TRUE)
                        res_xauto
                        res_xindeter <- try(p_resxindeter(), silent = TRUE)
                        res_xindeter
                        res_atermino <- try(p_res_atermino(), silent = TRUE)
                        res_atermino
                        res_luego1venc <- try(p_res_luego1venc(), silent = TRUE)
                        res_luego1venc
                        res_luego2venc <- try(p_res_luego2venc(), silent = TRUE)
                        res_luego2venc
                        prom_durac_inic_sent <- try(p_prom_durac_inic_sent(),
                                                    silent = TRUE)
                        prom_durac_inic_sent
                        durac_inic_sent_xproc <- try(p_durac_inic_sent_xproc(),
                                                     silent = TRUE)
                        durac_inic_sent_xproc
                        ccausa_ant <- try(p_ccausa_ant(), silent = TRUE)
                        ccausa_ant
                        fcausa_ant <- try(p_fcausa_ant(), silent = TRUE)
                        fcausa_ant
                        res_ccon <- try(p_resccon(), silent = TRUE)
                        res_ccon
                        sentmon_sinc <- try(p_sentmon_sinc(), silent = TRUE)
                        sentmon_sinc
                        sentmon_ccon <- try(p_sentmon_ccon(), silent = TRUE)
                        sentmon_ccon

                        # Arma df indicadores procesados
                        df <- data.frame(
                          causas_adespacho,
                          causas_resueltas,
                          res_xsentencia,
                          res_xauto,
                          res_xindeter,
                          res_atermino,
                          res_luego1venc,
                          res_luego2venc,
                          prom_durac_inic_sent,
                          durac_inic_sent_xproc,
                          ccausa_ant,
                          fcausa_ant,
                          res_ccon,
                          sentmon_sinc,
                          sentmon_ccon,
                          calidad_primaria)


                        # formateando fecha de causa más antigua
                        df$fcausa_ant <- as.Date(df$fcausa_ant, origin="1970-01-01")

                        # Evalúa calidad del informe presentado en base a consistencia de indicadores
                        # 1: Causas resueltas == resueltas x S + x A + x otra
                        # 2: Causas resoluestas == resueltas a termino + luego1v + luego2v
                        #     Como en estas columnas se presentan NA a veces no se puede evaluar la
                        #     la función de procesamiento

                        regla1 <- df$causas_resueltas == (df$res_xsentencia + df$res_xauto +
                                                            df$res_xindeter)
                        regla2 <- df$causas_resueltas == (df$res_atermino + df$res_luego1venc +
                                                            df$res_luego2venc)
                        df$calidad <- round(sum(regla1 + regla2, na.rm = T)/3, 2)


                        sentencias_pendientes <- tbs_prim_CADR1C_v2_L8 %>%
                          filter(is.na(fres), as =="S") %>%
                          mutate(sentencias_pendientes_vencidas = fvenc1 < lubridate::int_end(self$interval)) %>%
                          arrange(fdesp) %>%
                          dplyr::summarise(sentencias_pendientes = n(), sentencias_pendientes_vencidas = sum(sentencias_pendientes_vencidas), s_fdespacho_mas_antigua=fdesp[1], s_catatula_mas_antigua=caratula[1])

                        autos_pendientes <- tbs_prim_CADR1C_v2_L8 %>%
                          filter(is.na(fres), as =="A") %>%
                          mutate(autos_pendientes_vencidos = fvenc1 < lubridate::int_end(self$interval)) %>%
                          arrange(fdesp) %>%
                          dplyr::summarise(autos_pendientes = n(), autos_pendientes_vencidos = sum(autos_pendientes_vencidos), a_fdespacho_mas_antigua=fdesp[1], a_catatula_mas_antigua=caratula[1])

                        listado_pendientes <- tbs_prim_CADR1C_v2_L8 %>%
                          filter(is.na(fres)) %>%
                          mutate(vencido = fvenc1 < lubridate::int_end(self$interval)) %>%
                          arrange(fdesp)

                        pendientes <- cbind(sentencias_pendientes, autos_pendientes)
                        pendientes$total_adespacho <- causas_adespacho
                        pendientes <- pendientes %>%
                          mutate(total_pendientes = nrow(listado_pendientes)) %>%
                          mutate(total_pendientes_vencidas = listado_pendientes %>% filter(vencido) %>% nrow()) %>%
                          mutate(porcentaje_pendientes = total_pendientes / causas_adespacho )

                        if (nrow(pendientes)>0){
                          result$addResult(pendientes, name="pendientes")
                        }
                        if (nrow(listado_pendientes)>0){
                          result$addResult(listado_pendientes, name="listado_pendientes")
                        }

                        # Causas a despacho para el Boletin Oficial-----------------------------
                        adespacho <- data.frame(
                          nro =  tbs_prim_CADR1C_v2_L8$nro[is.na(tbs_prim_CADR1C_v2_L8$fres)],
                          caratula =  tbs_prim_CADR1C_v2_L8$caratula[is.na(tbs_prim_CADR1C_v2_L8$fres)],
                          fdesp = tbs_prim_CADR1C_v2_L8$fdesp[is.na(tbs_prim_CADR1C_v2_L8$fres)]
                        )

                        result$addResult(df)
                        if (nrow(adespacho)>0){
                          result$addResult(adespacho, name="adespacho")
                        }

                        result

                      }
                    )
)
