# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 23-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @export
LIDMF <- R6::R6Class("LIDMF",
                      inherit = Operation,
                      private = list(
                        processInput = function( tbs_prim_LIDMF ) {

                          result <- OperationResult$new()
                          colnames(tbs_prim_LIDMF) <- tolower(colnames(tbs_prim_LIDMF))

                          if (nrow(tbs_prim_LIDMF) == 0){
                            df <- data.frame()
                            attr(df, "empty") <- TRUE
                            result$addResult(df)

                            return(result)
                          }

                          # names set
                          colnames(tbs_prim_LIDMF) <- c("nro", "caratula", "so_estudio", "solicitante",
                                                     "tipo_intervencion", "fingreso", "fexamen_evaluacion",
                                                     "finforme", "examen_cod", "tec_evis_SN", "asistente_SN",
                                                     "profesional1", "profesional2", "profesional3")

                          calidad_primaria <- vector()
                          # VALIDACIONES

                          # errors: filter(!(is.na(nro) & is.na(caratula))) %>%

                          # VALIDAR QUE EL DNI DEL SUJETO ESTUDIO NO VENGA VACIO Ó ALGUN IDENTIFICADOR CUANDO NO SE DISPONGA

                          # a <- !is.na(tbs_prim_LIDMF$nro)
                          # if (checkWarn(all(a))){
                          #   e_vector <- tbs_prim_LIDMF %>%
                          #     filter(!a) %>% .$caratula %>% str_sub(0, 10)
                          #   erroneas <- paste0(e_vector, collapse = ", ")
                          #   mensaje <- "Existen registros sin Nro de Expediente cargado. Causas involucradas
                          #   (mostramos los 10 primeros caracteres de la carátula): "
                          #   result$addError("Nro de Expediente", paste0(mensaje, erroneas))
                          # }

                          b <- !is.na(tbs_prim_LIDMF$caratula)
                          if (checkWarn(all(b))){
                            e_vector <- tbs_prim_LIDMF %>%
                              filter(!b) %>% .$nro
                            erroneas <- paste0(e_vector, collapse = ", ")
                            mensaje <- "Existen registros sin Carátula cargada. Causas involucradas: "
                            result$addError("Carátula", paste0(mensaje, erroneas))
                          }

                          tbs_prim_LIDMF$fingreso <-
                            as.Date(tbs_prim_LIDMF$fingreso, format="%d/%m/%Y")

                          c <- tbs_prim_LIDMF %>%
                            filter(!is.na(fingreso)) %>% .$fingreso %>% self$withinInterval(TRUE)
                          if (checkWarn(all(c))){
                            e_vector <- tbs_prim_LIDMF[!is.na(tbs_prim_LIDMF$fingreso),] %>%
                              filter(!c) %>% .$nro
                            erroneas <- paste0(e_vector, collapse = ", ")
                            mensaje <- "La Fecha de Ingreso debe estar comprendida
                            en el período informado. Causas involucradas: "
                            result$addError("Fecha de Ingreso", paste0(mensaje, erroneas))
                          }

                          # dni <- as.integer(gsub("[^[:digit:]]", "", tbs_prim_LIDMF$so_estudio))
                          # Cambio obtención de DNI por venir muchos justiciables separados por %
                          dni <- tbs_prim_LIDMF %>%
                            tidyr::separate_rows(so_estudio, sep="\\%") %>%
                            filter(!so_estudio %in% c("", "$")) %>%
                            tidyr::separate(so_estudio, into = c("nombre", "dni"), sep="\\$") %>%
                            mutate( dni = as.integer(gsub("[^[:digit:]]", "", dni))) %>%
                            select(caratula, dni) %>%
                            group_by(caratula) %>%
                            filter(all(is.na(dni))) %>%
                            ungroup()

                          d <- !is.na(dni$dni) & (dni$dni == 0 | dni$dni > 1000000)
                          if (checkWarn(all(d))){
                            e_vector <- dni %>%
                              filter(!d) %>% .$caratula %>% str_sub(0, 15)
                            erroneas <- paste0(e_vector, collapse = ", ")
                            mensaje <- "Existen registros sin DNI o mal cargado. Debe
                            ser 0 (cero) ó mayor a 1 millón (1.000.000). Causas involucradas: "
                            result$addError("DNI", paste0(mensaje, erroneas))
                          }

                          x <- c(b, c, d)
                          calidad_primaria <- round(sum(x, na.rm = T)/length(x), 4)

                          # procesamiento

                          legajos_ingresados <- tbs_prim_LIDMF %>%
                            filter(!is.na(fingreso) & self$withinInterval(fingreso, TRUE)) %>%
                            filter(tipo_intervencion == 11) %>% # Tipo Administrativo
                            distinct(nro, caratula) %>%
                            nrow()

                          df <- data.frame(
                            calidad_primaria = calidad_primaria,
                            ingresados = legajos_ingresados
                          )

                          result$addResult(df)
                          result$addResult(tbs_prim_LIDMF, "primaria")


                          result
                        }
                      )
)
