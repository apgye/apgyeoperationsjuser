# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 23-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @export
CADUR1F <- R6::R6Class("CADUR1F",
                    inherit = Operation,
                    private = list(
                      processInput = function( tbs_prim_CADUR1F ) {
                        result <- OperationResult$new()
                        if (nrow(tbs_prim_CADUR1F) == 0){
                          df <- data.frame()
                          attr(df, "empty") <- TRUE
                          result$addResult(df)

                          return(result)
                        }

                        ##############  REEMPLAZAR IMPL ##################
                        df <- data.frame()
                        attr(df, "noproc") <- TRUE
                        result$addResult(df)

                        result
                      }
                    )
)
