# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 23-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @import lubridate
#' @importFrom stringr str_c
#' @export
CINC1C_v2_L8 <- R6::R6Class("CINC1C_v2_L8",
                  inherit = Operation,
                  private = list(
                    processInput = function(tbs_prim_CINC1C_v2_L8){

                      colnames(tbs_prim_CINC1C_v2_L8) <- tolower(colnames(tbs_prim_CINC1C_v2_L8))

                      result <- OperationResult$new()
                      if (nrow(tbs_prim_CINC1C_v2_L8) == 0){
                        df <- data.frame()
                        attr(df, "empty") <- TRUE
                        result$addResult(df)
                        return(result)
                      }

                      calidad_primaria <- 1

                      iniciados <- tbs_prim_CINC1C_v2_L8 %>%
                        mutate(finicio = lubridate::dmy(finicio)) %>%
                        filter(self$withinInterval(finicio)) %>%
                        nrow()

                      df <- data.frame(
                        calidad_primaria = calidad_primaria,
                        iniciados = iniciados
                      )

                      result$addResult(df)

                      result

                    }
                  )

)

