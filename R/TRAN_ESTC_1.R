# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 25-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero
# Old-Authors: Ing. Federico Caballaro, Lic. Paolo Orundés

## input: DataFrame to Process
## output: BD_TRAN_ESTC_1 actualization
## periodicity: On Demand
#' @export
TRAN_ESTC_1 <- R6::R6Class("TRAN_ESTC_1",
       inherit = Operation,
       private = list(
         processInput = function(tbs_prim_TRAN_ESTC_1){
          colnames(tbs_prim_TRAN_ESTC_1) <- tolower(colnames(tbs_prim_TRAN_ESTC_1))
          result <- OperationResult$new()

          if (nrow(tbs_prim_TRAN_ESTC_1) == 0){
            df <- data.frame()
            attr(df, "empty") <- TRUE
            result$addResult(df)
            return(result)
          }

          a <- !is.na(tbs_prim_TRAN_ESTC_1$estado)

          if (checkWarn(all(a))){
            e_vector <- tbs_prim_TRAN_ESTC_1 %>% filter(!a) %>% .$nro
            erroneas <- paste0(e_vector, collapse = ", ")
            mensaje <- "Existen Legajos sin Estado:"
            result$addWarning("Legajos sin Estado", paste0(mensaje, erroneas))
          }


         tbs_prim_TRAN_ESTC_1 <- tbs_prim_TRAN_ESTC_1 %>%
           mutate(estado = toupper(estado)) %>%
            group_by(estado) %>%
            summarise(cantidad_legajos = n()) %>%
            arrange(desc(cantidad_legajos)) %>%
            ungroup()


          result$addResult(tbs_prim_TRAN_ESTC_1)

          result

  }
))
