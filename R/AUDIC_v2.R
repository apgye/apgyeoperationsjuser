# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 24-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero
# Old-Authors: Ing. Federico Caballaro, Lic. Paolo Orundés

## input: DataFrame to Process
## output: BD_AUDIC_v2 actualization
## periodicity: On Demand
#' @export
AUDIC_v2 <- R6::R6Class("AUDIC_v2",
       inherit = Operation,
       private = list(
       processInput = function(tbs_prim_AUDIC_v2){


        # AJUSTE TEMPORAL HASTA RESOLVER DESDE LA BASE QUE NO GUARDE MAS COMO
        # TRUE LOS CAMPOS QUE VIENEN CON T O FALSE LOS QUE VIENEN CON F
        if (any(tbs_prim_AUDIC_v2$ra %in% c("TRUE","true"))) {
          tbs_prim_AUDIC_v2$ra <- as.character(tbs_prim_AUDIC_v2$ra)
          tbs_prim_AUDIC_v2$ra[tbs_prim_AUDIC_v2$ra %in% c("TRUE","true")] <- "T"
        }


        colnames(tbs_prim_AUDIC_v2) <- try(c("nro",
                                             "caratula",
                                             "tproc",
                                             "asis",
                                             "finicio",
                                             "duracm", #Error-g
                                             "duracb",
                                             "ffa", #Error-k
                                             "fea",
                                             "faprueb",
                                             "fprog",
                                             "ta", #Error-c
                                             "ra", #Error-e
                                             "esta",
                                             "jaud", #Error-j
                                             "oralidad",
                                             "audvid", #Error-f
                                             "justiables"))

        result <- OperationResult$new()
        if (nrow(tbs_prim_AUDIC_v2) == 0){
          df <- data.frame()
          attr(df, "empty") <- TRUE
          result$addResult(df)
          return(result)
        }

        # date format for use in withinInterval.
        tbs_prim_AUDIC_v2$fea <- as.Date(tbs_prim_AUDIC_v2$fea, format="%d/%m/%Y")

        inInterval <- !is.na(tbs_prim_AUDIC_v2$fea) & self$withinInterval(tbs_prim_AUDIC_v2$fea)
        realiz <- !is.na(tbs_prim_AUDIC_v2$esta) & tbs_prim_AUDIC_v2$esta == "2"

        ra_expected <- c("T", "P", "S", "t", "p", "s")

        calidad_primaria <- vector()
        a <- tbs_prim_AUDIC_v2 %>%
          filter(!is.na(asis) & inInterval & realiz) %>% .$asis
        a <- is.numeric(as.integer(a))

        if (checkWarn(all(a))){
          e_vector <- tbs_prim_AUDIC_v2 %>%
            filter(!is.na(asis)) %>%
            filter(!a) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "La cantidad de Asistentes -asis- debe ser un número.
                      Causas involucradas: "
          result$addWarning("Asistentes", paste0(mensaje, erroneas))
        }
        c <- tbs_prim_AUDIC_v2 %>%
          filter(!is.na(fea)) %>%
          .$ta %in% c(1:4)
        if (checkWarn(all(c))){
          e_vector <- tbs_prim_AUDIC_v2 %>%
            filter(!is.na(fea)) %>%
            filter(!c) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El Tipo de Audiencia -ta- debe estar comprendido
            entre 1 y 4. Causas involucradas: "
          result$addError("Tipos de Audiencia", paste0(mensaje, erroneas))
        }
        d <- tbs_prim_AUDIC_v2$esta %in% c(1:5)
        if (checkWarn(all(d))){
          e_vector <- tbs_prim_AUDIC_v2 %>%
            filter(!d) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El Estado de la Audiencia -esta- debe estar comprendido
            entre 1 y 5. Causas involucradas: "
          result$addError("Estado", paste0(mensaje, erroneas))
        }
        e <- tbs_prim_AUDIC_v2 %>%
          filter(inInterval & realiz) %>%
          filter(ta == "1") %>%
          .$ra %in% ra_expected
        if (checkWarn(all(e))){
          e_vector <- tbs_prim_AUDIC_v2 %>%
            filter(inInterval & realiz) %>%
            filter(ta == "1") %>%
            filter(!e) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Existen Audiencias mal declaradas
            según su Clasificación ('T', 'P' ó 'S'). Causas involucradas: "
          result$addError("Resultado", paste0(mensaje, erroneas))
        }
        f <- tbs_prim_AUDIC_v2$audvid %in% c(0, 1, NA, "")
        if (checkWarn(all(f))){
          e_vector <- tbs_prim_AUDIC_v2 %>%
            filter(!f) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Existen Audiencias mal declaradas
              según su Clasificación ('0' ó '1'). Causas involucradas: "
          result$addError("Videofilmadas", paste0(mensaje, erroneas))
        }

        g <- tbs_prim_AUDIC_v2 %>%
          filter(inInterval & realiz) %>%
          .$duracm %>% as.integer()
        g <- g/g # para una calidad primaria correcta
        if (anyNA(g)){
          e_vector <- tbs_prim_AUDIC_v2 %>%
            filter(inInterval & realiz) %>%
            filter(is.na(g)) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Para las Audiencias realizadas en el período, la
          Duración en minutos debe ser un número entero mayor
          a cero. Causas involucradas: "
          result$addError("Duración en Minutos", paste0(mensaje, erroneas))
        }

        h <- tbs_prim_AUDIC_v2 %>%
          filter(inInterval & realiz) %>%
          .$duracb %>% as.integer()
        h <- h/h # para una calidad primaria correcta
        if (anyNA(h)){
          e_vector <- tbs_prim_AUDIC_v2 %>%
            filter(inInterval & realiz) %>%
            filter(is.na(h)) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Para las Audiencias realizadas en el período, la
          Cantidad de Bloques debe ser un número entero mayor
          a cero. Causas involucradas: "
          result$addError("Cantidad de Bloques", paste0(mensaje, erroneas))
        }

        if ((month(int_start(self$interval))+1 == month(Sys.Date()) |
             month(int_start(self$interval)) == 12 & month(Sys.Date()) %in% c(1,2)) & #add by T2328
            exists("id_agentes_gbl")){
          magist_func_id_agentes <- id_agentes_gbl

        } else {
          # buscaremos los id_agentes para el período a presentar. caso Fam2Pna(255-Solari)
          magist_func_id_agentes <- magistrados_y_secretarios_periodo(DB_PROD(),
                                                                      int_start(self$interval),
                                                                      int_end(self$interval)) %>%
            pull(idagente)

          # add "0" ti IDs for External Agent
          magist_func_id_agentes[length(magist_func_id_agentes)+1] <- "0"
        }

        # JAUD VALIDATION
        j <- tbs_prim_AUDIC_v2 %>%
          filter(realiz) %>%
          .$jaud %in% magist_func_id_agentes
        if (checkWarn(all(j))){
          e_vector <- tbs_prim_AUDIC_v2 %>%
            filter(realiz) %>%
            filter(!j) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Para Audiencias Realizadas -esta = 2-,
            existe/n código/s declarados para el Juez
            no reconocidos o faltantes. Causas involucradas: "
          result$addError("Código del Juez", paste0(mensaje, erroneas))
        }
        # Validacion Fechas
        k <- !is.na(tbs_prim_AUDIC_v2$ffa)
        if (checkWarn(all(k))){
          e_vector <- tbs_prim_AUDIC_v2 %>%
            filter(!k) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Existen Audiencias sin Fecha
                            de Fijación cargada. Causas involucradas: "
          result$addError("Fecha Fijación", paste0(mensaje, erroneas))
        }
        # TODO: Validar cuando estado distinto '1' ó !na la fecha
        # de efectivizacion no sea nula/vacia
        l <- !is.na(tbs_prim_AUDIC_v2[!is.na(tbs_prim_AUDIC_v2$esta) &
                                 tbs_prim_AUDIC_v2$esta != "1",fea])
        if (checkWarn(all(l))){
          e_vector <- tbs_prim_AUDIC_v2[!is.na(tbs_prim_AUDIC_v2$esta) &
                                          tbs_prim_AUDIC_v2$esta != "1",] %>%
                                        filter(!l) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Existen Audiencias sin
              Fecha de Efectivización cargada. Causas involucradas: "
          result$addError("Fecha Efectivización", paste0(mensaje, erroneas))
        }
        ambas <- !is.na(tbs_prim_AUDIC_v2$ffa) &
                 !is.na(tbs_prim_AUDIC_v2$fea)
        m <- tbs_prim_AUDIC_v2[ambas,ffa] <= tbs_prim_AUDIC_v2[ambas,fea]
        # En casos de Ausencia de fechas, "m devuelve NA"
        if (checkWarn(all(m))){
          e_vector <- tbs_prim_AUDIC_v2[ambas,] %>%
            filter(!m) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "Existen Causas con Fecha de Fijación posterior a la
                Fecha de Efectivización. Causas involucradas: "
          result$addError("Fecha de Fijación posterior", paste0(mensaje, erroneas))
        }

        x <- c(a, c, d, e, f, g, h, j, k, l, m)
        calidad_primaria <- round(sum(x, na.rm = T)/length(x), 4)

        # Preparar tbs primarias: Asignar clase a columnas fechas, Agregar columna de
        # mes de dictado de resolución. Convertir a mayúsculas as
          tbs_prim_AUDIC_v2$finicio <-
            as.Date(tbs_prim_AUDIC_v2$finicio, format="%d/%m/%Y")
          tbs_prim_AUDIC_v2$ffa <-
            as.Date(tbs_prim_AUDIC_v2$ffa, format="%d/%m/%Y")
          tbs_prim_AUDIC_v2$mes_ffa <-
            month(tbs_prim_AUDIC_v2$ffa)
          tbs_prim_AUDIC_v2$mes_fea <-
            month(tbs_prim_AUDIC_v2$fea)
          tbs_prim_AUDIC_v2$ra <-
            toupper(tbs_prim_AUDIC_v2$ra)
          tbs_prim_AUDIC_v2$oralidad <-
            toupper(tbs_prim_AUDIC_v2$oralidad)
          tbs_prim_AUDIC_v2$duracm <-
            as.integer(gsub("[^0-9]", "", tbs_prim_AUDIC_v2$duracm))
          tbs_prim_AUDIC_v2$duracb <-
            as.integer(gsub("[^0-9]", "", tbs_prim_AUDIC_v2$duracb))

        # Creación de funciones

        p_aud_fijadas_total <- function() {
          # Cuenta audiencias informadas (total)
          aud_fijadas_total <- nrow(tbs_prim_AUDIC_v2)
          aud_fijadas_total
        }

        p_aud_fijadas <- function() {
          # Cuenta audiencias fijadas en el mes
          aud_fijadas <- sum(self$withinInterval(tbs_prim_AUDIC_v2$ffa), na.rm = T)
          aud_fijadas

        }

        p_aud_realizadas <- function() {
          # Cuenta audiencias realizadas en el mes
          aud_realizadas <- sum(inInterval &
                                     tbs_prim_AUDIC_v2$esta == 2, na.rm = T)
          aud_realizadas

        }

        p_aud_norealizadas <- function() {
          # Cuenta audiencias no realizadas en el mes
          aud_norealizadas <- sum(inInterval &
                                      tbs_prim_AUDIC_v2$esta == 3, na.rm = T)
          aud_norealizadas

        }

        p_aud_canceladas <- function() {
          # Cuenta audiencias canceladas en el mes
          aud_canceladas <- sum(inInterval &
                                      tbs_prim_AUDIC_v2$esta == 4, na.rm = T)
          aud_canceladas

        }

        p_aud_reprogramadas <- function() {
          # Cuenta audiencias reprogramadas en el mes
          aud_reprogramadas <- sum(inInterval &
                                      tbs_prim_AUDIC_v2$esta == 5, na.rm = T)
          aud_reprogramadas

        }


        p_aud_preliminar <- function() {
          # Cuenta audiencias preliminares realizadas en el mes
          aud_preliminar <- sum(inInterval &
                                      tbs_prim_AUDIC_v2$esta == 2 &
                                      tbs_prim_AUDIC_v2$ta == 1, na.rm = T)
          aud_preliminar

        }

        p_aud_vista_causa <- function() {
          # Cuenta audiencias de prueba realizadas en el mes
          aud_vista_causa <- sum(inInterval &
                                      tbs_prim_AUDIC_v2$esta == 2 &
                                      tbs_prim_AUDIC_v2$ta == 2, na.rm = T)
          aud_vista_causa

        }

        p_aud_conciliacion <- function() {
          # Cuenta audiencias conciliación realizadas en el mes
          aud_conciliacion <- sum(inInterval &
                                      tbs_prim_AUDIC_v2$esta == 2 &
                                      tbs_prim_AUDIC_v2$ta == 3, na.rm = T)
          aud_conciliacion

        }

        p_aud_otras <- function() {
          # Cuenta audiencias otras realizadas en el mes
          aud_otras <- sum(inInterval &
                                        tbs_prim_AUDIC_v2$esta == 2 &
                                        tbs_prim_AUDIC_v2$ta == 4, na.rm = T)
          aud_otras

        }

        p_aud_conct <- function() {
          # Cuenta audiencias con conciliación total
          aud_conct <- sum(inInterval &
                                 tbs_prim_AUDIC_v2$ra == "T", na.rm = T)
          aud_conct

        }

        p_aud_concp <- function() {
          # Cuenta audiencias con conciliación parcial
          aud_concp <- sum(inInterval &
                                 tbs_prim_AUDIC_v2$ra == "P", na.rm = T)
          aud_concp

        }

        p_aud_sconc <- function() {
          # Cuenta audiencias sin conciliacion
          aud_sconc <- sum(inInterval &
                                 tbs_prim_AUDIC_v2$ra == "S", na.rm = T)
          aud_sconc

        }

        p_prom_durac_ffa_frealizada <- function() {
          # Duración desde Fecha de Fijación a F.Realización
          indice_aud_realizadas <- inInterval &
            tbs_prim_AUDIC_v2$esta == 2
          durac_ffa_frealizada <-
            mean(as.integer(tbs_prim_AUDIC_v2$fea[indice_aud_realizadas] -
                              tbs_prim_AUDIC_v2$ffa[indice_aud_realizadas]),
                 na.rm = T)
          durac_ffa_frealizada
        }

        p_prom_asis <- function() {
          # Promedio de asistentes por audiencia
          indice_aud_realizadas <- inInterval &
            tbs_prim_AUDIC_v2$esta == 2
          prom_asis <- mean(tbs_prim_AUDIC_v2$asis[indice_aud_realizadas],
                 na.rm = T)
          prom_asis
        }

        p_prom_duracm <- function() {
          # Promedio de duración en minutos por audiencia
          indice_aud_realizadas <- inInterval &
            tbs_prim_AUDIC_v2$esta == 2
          prom_duracm <- mean(tbs_prim_AUDIC_v2$duracm[indice_aud_realizadas],
                            na.rm = T)
          prom_duracm
        }

        p_mediana_duracm <- function() {
          # Promedio de duración en minutos por audiencia
          indice_aud_realizadas <- inInterval &
            tbs_prim_AUDIC_v2$esta == 2
          mediana_duracm <- median(tbs_prim_AUDIC_v2$duracm[indice_aud_realizadas],
                              na.rm = T)
          mediana_duracm
        }

        p_prom_duracb <- function() {
          # Promedio de duración en minutos por audiencia
          indice_aud_realizadas <- inInterval &
            tbs_prim_AUDIC_v2$esta == 2
          prom_duracb <- mean(tbs_prim_AUDIC_v2$duracb[indice_aud_realizadas],
                              na.rm = T)
          prom_duracb
        }

        p_aud_videofilm <- function() {
          # Cuenta audiencias preliminar en el mes
          aud_videofilm <- sum(inInterval &
                                      tbs_prim_AUDIC_v2$esta == 2 &
                                      tbs_prim_AUDIC_v2$audvid == 1, na.rm = T)
          aud_videofilm
        }

        p_aud_matfam <- function() {
          # Cuenta audiencias reprogramadas en el mes
          aud_matfam <- sum(inInterval &
                              tbs_prim_AUDIC_v2$esta == 2 &
                                 tbs_prim_AUDIC_v2$oralidad == "F", na.rm = T)
          aud_matfam
        }

        p_aud_matciv <- function() {
          # Cuenta audiencias reprogramadas en el mes
          aud_matciv <- sum(inInterval &
                              tbs_prim_AUDIC_v2$esta == 2 &
                              tbs_prim_AUDIC_v2$oralidad == "C", na.rm = T)
          aud_matciv
        }

        p_aud_matlab <- function() {
          # Cuenta audiencias reprogramadas en el mes
          aud_matlab <- sum(inInterval &
                              tbs_prim_AUDIC_v2$esta == 2 &
                              tbs_prim_AUDIC_v2$oralidad == "L", na.rm = T)
          aud_matlab
        }

        p_aud_matpen <- function() {
          # Cuenta audiencias reprogramadas en el mes
          aud_matpen <- sum(inInterval &
                              tbs_prim_AUDIC_v2$esta == 2 &
                              tbs_prim_AUDIC_v2$oralidad == "P", na.rm = T)
          aud_matpen
        }

        # DESARROLLO:
        # Audiencias y tipo de proceso!

        # crear vectores
        aud_fijadas_total <- vector()
        aud_fijadas <- vector()
        aud_realizadas <- vector()
        aud_norealizadas <- vector()
        aud_canceladas <- vector()
        aud_reprogramadas <- vector()
        aud_preliminar <- vector()
        aud_vista_causa <- vector()
        aud_conciliacion <- vector()
        aud_otras <- vector()
        aud_conct <- vector()
        aud_concp <- vector()
        aud_sconc <- vector()
        durac_ffa_frealizada <- vector()
        prom_asis <- vector()
        prom_duracm <- vector()
        mediana_duracm <- vector()
        prom_duracb <- vector()
        aud_videofilm <- vector()
        aud_matfam <- vector()
        aud_matciv <- vector()
        aud_matlab <- vector()
        aud_matpen <- vector()


        # Extracción de indicadores estadísticos.Con "try" para devolver NA ante
        # falla
          aud_fijadas_total <- try(p_aud_fijadas_total(), silent = TRUE)
          aud_fijadas_total
          aud_fijadas <- try(p_aud_fijadas(), silent = TRUE)
          aud_fijadas
          aud_realizadas <- try(p_aud_realizadas(), silent = TRUE)
          aud_realizadas
          aud_norealizadas <- try(p_aud_norealizadas(), silent = TRUE)
          aud_norealizadas
          aud_canceladas <- try(p_aud_canceladas(), silent = TRUE)
          aud_canceladas
          aud_reprogramadas <- try(p_aud_reprogramadas(), silent = TRUE)
          aud_reprogramadas
          aud_preliminar <- try(p_aud_preliminar(), silent = TRUE)
          aud_preliminar
          aud_vista_causa <- try(p_aud_vista_causa(), silent = TRUE)
          aud_vista_causa
          aud_conciliacion <- try(p_aud_conciliacion(), silent = TRUE)
          aud_conciliacion
          aud_otras <- try(p_aud_otras(), silent = TRUE)
          aud_otras
          aud_conct <- try(p_aud_conct(), silent = TRUE)
          aud_conct
          aud_concp <- try(p_aud_concp(), silent = TRUE)
          aud_concp
          aud_sconc <- try(p_aud_sconc(), silent = TRUE)
          aud_sconc
          durac_ffa_frealizada <- try(p_prom_durac_ffa_frealizada(), silent = TRUE)
          durac_ffa_frealizada
          prom_asis <- try(p_prom_asis(), silent = TRUE)
          prom_asis
          prom_duracm <- try(p_prom_duracm(), silent = TRUE)
          prom_duracm
          mediana_duracm <- try(p_mediana_duracm(), silent = TRUE)
          mediana_duracm
          prom_duracb <- try(p_prom_duracb(), silent = TRUE)
          prom_duracb
          aud_videofilm <- try(p_aud_videofilm(), silent = TRUE)
          aud_videofilm
          aud_matfam <- try(p_aud_matfam(), silent = TRUE)
          aud_matfam
          aud_matciv <- try(p_aud_matciv(), silent = TRUE)
          aud_matciv
          aud_matlab <- try(p_aud_matlab(), silent = TRUE)
          aud_matlab
          aud_matpen <- try(p_aud_matpen(), silent = TRUE)
          aud_matpen

        # Arma df indicadores procesados
        df <- data.frame(
          aud_fijadas_total,
          aud_fijadas,
          aud_realizadas,
          aud_norealizadas,
          aud_canceladas,
          aud_reprogramadas,
          aud_preliminar,
          aud_vista_causa,
          aud_conciliacion,
          aud_otras,
          aud_conct,
          aud_concp,
          aud_sconc,
          durac_ffa_frealizada,
          prom_asis,
          prom_duracm,
          mediana_duracm,
          prom_duracb,
          aud_videofilm,
          aud_matfam,
          aud_matciv,
          aud_matlab,
          aud_matpen,
          calidad_primaria)

        # Evalúa calidad del informe presentado en base a consistencia de indicadores
        # TODO: CONSULTAR A SEBA POR VALIDACIONES DE CALIDAD EN AUDIENCIAS
         regla1 <- aud_fijadas == (aud_realizadas
                                       + aud_norealizadas)

         regla2 <- aud_norealizadas == (aud_canceladas
                                            + aud_reprogramadas)

         df$calidad <- round(sum(regla1, regla2, na.rm = T)/2, 2)

         result$addResult(df)

         result
       }

       ))






