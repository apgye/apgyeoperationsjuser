# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 25-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero
# Old-Authors: Ing. Federico Caballaro, Lic. Paolo Orundés

## input: DataFrame to Process
## output: BD_CAMOV actualization
## periodicity: On Demand
#' @export
CAMOV <- R6::R6Class("CAMOV",
       inherit = Operation,
       private = list(
         processInput = function(tbs_prim_CAMOV){
          colnames(tbs_prim_CAMOV) <- tolower(colnames(tbs_prim_CAMOV))
          result <- OperationResult$new()
          if (nrow(tbs_prim_CAMOV) == 0){
            df <- data.frame()
            attr(df, "empty") <- TRUE
            result$addResult(df)
            return(result)
          }

          # Asigamos 1 a la Primaria (anteriormente ya se validó la cantidad
          # de columnas del archivo)
          calidad_primaria <- 1

          causas_con_movimientos <- tbs_prim_CAMOV %>%
            mutate(fmov = lubridate::dmy(fmov)) %>%
            filter(self$withinInterval(fmov)) %>%
            nrow()

          movs_DF <- tbs_prim_CAMOV %>%
            tidyr::separate_rows(movt, sep='%') %>%
            tidyr::separate(movt, c("fecha", "movimiento"), sep="\\$") %>%
            mutate(fecha = lubridate::dmy(fecha)) %>%
            filter(self$withinInterval(fecha))


          movimientos <- movs_DF %>%
            nrow()


          df <- data.frame(
            calidad_primaria = calidad_primaria,
            causas_con_movimientos = causas_con_movimientos,
            movimientos_totales = movimientos
          )

          desagregados_por_tproc <- movs_DF %>%
            group_by(tproc) %>%
            summarise(cantidad = n())

          result$addResult(df)

          result$addResult(desagregados_por_tproc, "por_tipo_proceso")

          result

  }
))
