# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 26-06-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @import dplyr
#' @import apgyeCommon
#' @export
CARCHCAD <- R6::R6Class("CARCHCAD",
                    inherit = Operation,
                    private = list(
                      processInput = function( tbs_prim_CARCHCAD ) {
                        result <- OperationResult$new()
                        if (nrow(tbs_prim_CARCHCAD) == 0){
                          df <- data.frame()
                          attr(df, "empty") <- TRUE
                          result$addResult(df)

                          return(result)
                        }

                        calidad_primaria <- 1

                        archivados <- tbs_prim_CARCHCAD %>% nrow()

                        df <- data.frame(
                          calidad_primaria = calidad_primaria,
                          archivados = archivados
                        )

                        result$addResult(df)

                        result
                      }
                    )
)
