library(dplyr)
Tipo_procesos <- apgyeOperationsJusER::Tipo_procesos
Tipo_procesos_lab <- Tipo_procesos %>%
  filter(materia == "laboral")

tipo_procesos_conocimiento_lab <- c(
  'ACCIDENTE',
  'ACCION DE AMPARO',
  'ACCION DE EJECUCION',
  'ACCION MERAMENTE DECLARATIVA',
  'ACCION DE INCONSTITUCIONALIDAD',
  'AMPARO SINDICAL',
  'APEL.ART.46',
  'APELACION DICTAMEN DE COMISION',
  'COBRO DE PESOS',
  'COBRO DE CUOTA SINDICAL',
  'CONSIGNACION',
  'MEDIDA AUTOSATISFACTIVA',
  'DEMANDA AUTONOMA',
  'ENFERMEDAD PROFESIONAL',
  'EXCLUSION DE TUTELA SINDICAL',
  'INDEMNIZACION POR DAÑOS Y PERJUICIOS',
  'INDEM',
  'ORDINARIO',
  'QUERELLA POR PRACTICA DESLEAL',
  'REVISION DE SANCION DISCIPLINARIA',
  'SUMARIO',
  'SUMARISIMO',
  'TERCERIA DE DOMINIO')


procesos_conoc_laborales <- procesos_conoc_laborales %>%
  mutate(materia = "laboral",
         codigo_procesos = NA,
         tipo_de_procesos = tproc,
         Tipo_tramites_turnovoluntario = NA,
         Penal_instanciayTipo_delito = NA,
         proceso_conocimiento = NA) %>%
  filter(!tproc %in% Tipo_procesos_lab$tipo_de_procesos) %>%
  select(colnames(Tipo_procesos))

Tipo_procesos_lab <- bind_rows(Tipo_procesos_lab, procesos_conoc_laborales)
conocimiento_lab <- purrr::map(Tipo_procesos_lab$tipo_de_procesos,
                                   startsWith, tipo_procesos_conocimiento_lab) %>%
  purrr::map_lgl(any)
Tipo_procesos_lab$proceso_conocimiento <- conocimiento_lab

Tipo_procesos <- Tipo_procesos %>%
  filter(materia != "laboral")

Tipo_procesos <- bind_rows(Tipo_procesos, Tipo_procesos_lab)

devtools::use_data(Tipo_procesos, overwrite = TRUE)


df <- apgyeOperationsJusER::Tipo_procesos
