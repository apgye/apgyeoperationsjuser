# Prueba de Operaciones

source("../apgyeinformes/R/informe.R")

# Creamos intervalo
start <- "2020-01-01"
end <- "2020-02-01"
intervalo <- interval(start, end)


# Iniciamos la Operacion
LIDMF$new(intervalo)

# Asignar un set de datos al tbs_prim_'operacion'

tbs_prim_LIDMF <- read.delim("../PRUEBAS/datos/DMF_IngresoEnero2021.txt", header = T,
                             sep = "\t", stringsAsFactors = F, encoding = "latin1")

view(tbs_prim_LIDMF)

# Ir a la Operacion y probar

LIDMF(tbs_prim_LIDMF)
