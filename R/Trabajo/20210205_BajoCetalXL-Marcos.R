library(dplyr)
library(apgyeOperationsJusER)
daoPROD <- apgyeDataAccess::SubmissionsDAO$new(DB_PROD())


sub_CetalCiv2Uru <- presentaciones_justat %>%
  filter(user_id == "28676532" &
           iep == "jdocco0200uru") %>%
  filter(enabled == TRUE) %>%
  .$id_submission

daoPROD$disable(id_submission = sub_CetalCiv2Uru, user_id = 28676532,
                  reason = "Anulamos por presentar probando el archivo del usuario. Taiga 1225",
                  enablePrev = FALSE)

