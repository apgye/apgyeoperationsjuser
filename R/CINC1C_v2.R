# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 23-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @import lubridate
#' @importFrom stringr str_c
#' @export
CINC1C_v2 <- R6::R6Class("CINC1C_v2",
                  inherit = Operation,
                  private = list(
                    processInput = function(tbs_prim_CINC1C_v2){

                      colnames(tbs_prim_CINC1C_v2) <- tolower(colnames(tbs_prim_CINC1C_v2))

                      result <- OperationResult$new()
                      if (nrow(tbs_prim_CINC1C_v2) == 0){
                        df <- data.frame()
                        attr(df, "empty") <- TRUE
                        result$addResult(df)
                        return(result)
                      }

                      tbs_prim_CINC1C_v2 <- tbs_prim_CINC1C_v2 %>%
                        mutate(finicio = lubridate::dmy(finicio))

                      iniciados <- tbs_prim_CINC1C_v2 %>%
                        filter(self$withinInterval(finicio, TRUE)) %>%
                        nrow()


                      a <- tbs_prim_CINC1C_v2 %>%
                        filter(!is.na(finicio)) %>% .$finicio %>% self$withinInterval()
                      if (checkWarn(all(a))){
                        e_vector <- tbs_prim_CINC1C_v2 %>%
                          filter(!is.na(finicio)) %>%
                          filter(!a) %>% .$nro
                        erroneas <- paste0(e_vector, collapse = ", ")
                        mensaje <- "Las Fechas de Inicio deben estar
                            comprendidas en el Período Informado. Causas involucradas: "
                        result$addError("Fecha de Inicio", paste0(mensaje, erroneas))
                      }

                      b <- vector()
                      c <- vector()
                      #Ajuste por los CyQ de Concordia
                      if (!is.na(submission_iep) &&
                          submission_iep %in% c("jdocco0501con","jdocco0502con")){
                        # Controlar que la Sec1 informe Expedientes Impares y
                        # la Sec2 informe Expedientes Pares

                        is.even <- function(x) x %% 2 == 0
                        if (submission_iep == "jdocco0501con"){
                          bb <- tbs_prim_CINC1C_v2 %>%
                            filter(!is.na(nro)) %>% filter(!is.na(as.integer(nro)))
                          b <- !is.even(as.integer(bb$nro))
                          if (checkWarn(all(b))){
                            e_vector <- tbs_prim_CINC1C_v2 %>%
                              filter(!is.na(nro)) %>% filter(!is.na(as.integer(nro))) %>%
                              filter(!b) %>% .$nro
                            erroneas <- paste0(e_vector, collapse = ", ")
                            mensaje <- "Existen expedientes No Impares.
                            Se han hallado números de Expedientes que no
                            son un número impar. Causas involucradas: "
                            result$addError("Número de Expediente", paste0(mensaje, erroneas))
                          }
                        }
                        if (submission_iep == "jdocco0502con"){
                          cc <- tbs_prim_CINC1C_v2 %>%
                            filter(!is.na(nro)) %>% filter(!is.na(as.integer(nro)))
                          c <- is.even(as.integer(cc$nro))
                          if (checkWarn(all(c))){
                            e_vector <- tbs_prim_CINC1C_v2 %>%
                              filter(!is.na(nro)) %>% filter(!is.na(as.integer(nro))) %>%
                              filter(!c) %>% .$nro
                            erroneas <- paste0(e_vector, collapse = ", ")
                            mensaje <- "Existen expedientes No Pares.
                            Se han hallado números de Expedientes que no
                            son un número par. Causas involucradas: "
                            result$addError("Número de Expediente", paste0(mensaje, erroneas))
                          }
                        }

                      }

                      x <- c(a, b, c)
                      calidad_primaria <- round(sum(x, na.rm = T)/length(x), 4)

                      df <- data.frame(
                        calidad_primaria = calidad_primaria,
                        iniciados = iniciados
                      )

                      result$addResult(df)

                      result

                    }
                  )

)

