# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 24-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero
# Old-Authors: Ing. Federico Caballaro, Lic. Paolo Orundés

## input: DataFrame to Process
## output: BD_AP_SAT_AB actualization
## periodicity: On Demand
#' @export
AP_SAT_AB <- R6::R6Class("AP_SAT_AB",
       inherit = Operation,
       private = list(
       processInput = function(tbs_prim_AP_SAT_AB){

        colnames(tbs_prim_AP_SAT_AB) <- try(c("nro", "caratula", "tproc",
                                             "realizada",	"trato",	"depurac_prueba",
                                             "intent_concil", "instalaciones"))

        result <- OperationResult$new()
        if (nrow(tbs_prim_AP_SAT_AB) == 0){
          df <- data.frame()
          attr(df, "empty") <- TRUE
          result$addResult(df)
          return(result)
        }

        tbs_prim_AP_SAT_AB$realizada <- as.Date(tbs_prim_AP_SAT_AB$realizada, format="%d/%m/%Y")
        inInterval <- !is.na(tbs_prim_AP_SAT_AB$realizada) &
          self$withinInterval(tbs_prim_AP_SAT_AB$realizada)

        calidad_primaria <- vector()

        aa <- tbs_prim_AP_SAT_AB %>%
          filter(!is.na(realizada)) %>% .$realizada %>% self$withinInterval()
        if (checkWarn(all(aa))){
          e_vector <- tbs_prim_AP_SAT_AB %>%
            filter(!is.na(realizada)) %>%
            filter(!aa) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "La Fecha de realización debe estar comprendida
                      en el período informado. Causas involucradas: "
          result$addError("Fecha de realización", paste0(mensaje, erroneas))
        }

        a <- tbs_prim_AP_SAT_AB %>%
          .$trato %in% c(0:4)
        if (checkWarn(all(a))){
          e_vector <- tbs_prim_AP_SAT_AB %>%
            filter(!a) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El valor de Trato debe estar comprendido
            entre 0 y 4. Causas involucradas: "
          result$addError("Trato", paste0(mensaje, erroneas))
        }

        b <- tbs_prim_AP_SAT_AB %>%
          .$depurac_prueba %in% c(0:4)
        if (checkWarn(all(b))){
          e_vector <- tbs_prim_AP_SAT_AB %>%
            filter(!b) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El valor de Depuración Prueba debe estar comprendido
            entre 0 y 4. Causas involucradas: "
          result$addError("Depuración Prueba", paste0(mensaje, erroneas))
        }

        c <- tbs_prim_AP_SAT_AB %>%
          .$intent_concil %in% c(0:4)
        if (checkWarn(all(c))){
          e_vector <- tbs_prim_AP_SAT_AB %>%
            filter(!c) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El valor de Intento Conciliación debe estar comprendido
            entre 0 y 4. Causas involucradas: "
          result$addError("Intento Conciliación", paste0(mensaje, erroneas))
        }

        d <- tbs_prim_AP_SAT_AB %>%
          .$instalaciones %in% c(0:4)
        if (checkWarn(all(d))){
          e_vector <- tbs_prim_AP_SAT_AB %>%
            filter(!d) %>% .$nro
          erroneas <- paste0(e_vector, collapse = ", ")
          mensaje <- "El valor de Instalaciones debe estar comprendido
            entre 0 y 4. Causas involucradas: "
          result$addError("Instalaciones", paste0(mensaje, erroneas))
        }

        # Validacion Fecha
        e <- try(all(!is.null(tbs_prim_AP_SAT_AB$realizada) |
                   !is.na(tbs_prim_AP_SAT_AB$realizada)))
        if (checkWarn(e)){
          result$addError("Fecha Realizacion", "Existen encuestas sin Fecha
                            de realización de audiencia.")
        }

        x <- c(aa, a, b, c, d, e)
        calidad_primaria <- round(sum(x, na.rm = T)/length(x), 4)

        # Preparar tbs primarias: Asignar clase a columnas fechas, Agregar columna de
        # mes de dictado de resolución. Convertir a mayúsculas as
          tbs_prim_AP_SAT_AB$realizada <-
            as.Date(tbs_prim_AP_SAT_AB$realizada, format="%d/%m/%Y")
         tbs_prim_AP_SAT_AB$trato <-
            as.integer(tbs_prim_AP_SAT_AB$trato)
         tbs_prim_AP_SAT_AB$depurac_prueba <-
           as.integer(tbs_prim_AP_SAT_AB$depurac_prueba)
         tbs_prim_AP_SAT_AB$intent_concil <-
           as.integer(tbs_prim_AP_SAT_AB$intent_concil)
         tbs_prim_AP_SAT_AB$instalaciones <-
           as.integer(tbs_prim_AP_SAT_AB$instalaciones)


        # Procesar
         df <- tbs_prim_AP_SAT_AB %>%

           mutate(trato = ifelse(trato == 0, NA, trato),
                  intent_concil = ifelse(intent_concil == 0, NA, intent_concil),
                  depurac_prueba = ifelse(depurac_prueba == 0, NA, depurac_prueba),
                  instalaciones = ifelse(instalaciones == 0, NA, instalaciones)) %>%
           summarise_if(is.numeric, mean, na.rm=TRUE) %>%
           mutate(calidad_primaria = calidad_primaria)

         result$addResult(df)

         result
       }

       ))







