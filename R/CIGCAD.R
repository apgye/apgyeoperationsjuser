# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 26-06-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero

## input: DataFrame to Process
## output: OperationResult
#' @import dplyr
#' @import apgyeCommon
#' @export
CIGCAD <- R6::R6Class("CIGCAD",
                    inherit = Operation,
                    private = list(
                      processInput = function( tbs_prim_CIGCAD ) {

                        result <- OperationResult$new()
                        colnames(tbs_prim_CIGCAD) <- tolower(colnames(tbs_prim_CIGCAD))

                        if (nrow(tbs_prim_CIGCAD) == 0){
                          df <- data.frame()
                          attr(df, "empty") <- TRUE
                          result$addResult(df)

                          return(result)
                        }

                        calidad_primaria <- 1

                        iniciados <- tbs_prim_CIGCAD %>%
                          tidyr::separate(exptac, c("exptac1", "fingreso"), sep = "[[\\$]]") %>%
                          mutate(fingreso = lubridate::dmy(fingreso)) %>%
                          filter(self$withinInterval(fingreso, TRUE)) %>%
                          nrow()

                        df <- data.frame(
                          calidad_primaria = calidad_primaria,
                          iniciados = iniciados
                        )

                        result$addResult(df)

                        result
                      }
                    )
)
