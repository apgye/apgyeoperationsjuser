# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 25-01-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero
# Old-Authors: Ing. Federico Caballaro, Lic. Paolo Orundés

## input: DataFrame to Process
## output: BD_CETAL_XL_L8 actualization
## periodicity: On Demand
#' @export
CETAL_XL_L8 <- R6::R6Class("CETAL_XL_L8",
       inherit = Operation,
       private = list(
         processInput = function(tbs_prim_CETAL_XL_L8){

          colnames(tbs_prim_CETAL_XL_L8) <- tolower(colnames(tbs_prim_CETAL_XL_L8))

          result <- OperationResult$new()

          if (nrow(tbs_prim_CETAL_XL_L8) == 0){
            df <- data.frame()
            attr(df, "empty") <- TRUE
            result$addResult(df)
            return(result)
          }

          # Asigamos 1 a la Primaria ?? (anteriormente ya se validó la cantidad
          # de columnas del archivo)
          calidad_primaria <- 1

          # causas_con_movimientos <- tbs_prim_CETAL_XL_L8 %>%
          #   mutate(fmov = lubridate::dmy(fmov)) %>%
          #   filter(self$withinInterval(fmov)) %>%
          #   nrow()
          #
          # movs_DF <- tbs_prim_CETAL_XL_L8 %>%
          #   tidyr::separate_rows(movt, sep='%') %>%
          #   tidyr::separate(movt, c("fecha", "movimiento"), sep="\\$") %>%
          #   mutate(fecha = lubridate::dmy(fecha)) %>%
          #   filter(self$withinInterval(fecha))

          en_tramite <- tbs_prim_CETAL_XL_L8 %>% nrow()

          df <- data.frame(
            calidad_primaria = calidad_primaria,
            cantidad_filas = en_tramite
          )

          result$addResult(df)

          result

  }
))
