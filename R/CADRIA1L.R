# JUSTAT is a free software developed for official statistics of Supreme Court
# of Entre Ríos (STJER), Argentina, Office of Planification Management and
# Statistics (APGE)

# V.2.0
# 05-02-2018

# Authors
# JUSTAT: Lic. Sebastián Castillo, Bioing. Zacarías Ojeda y Lic. Marcos Londero
# Old-Authors: Ing. Federico Caballaro, Lic. Paolo Orundés

## input: DataFrame to Process
## output: BD_CADRIA1L actualization
## periodicity: On Demand
#' @export
CADRIA1L <- R6::R6Class("CADRIA1L",
        inherit = Operation,
        private = list(
        processInput = function(tbs_prim_CADRIA1L, causas_inic_ppales,
                                causas_inic_noppales, causas_archivadas
        ) {

          causas_inic_ppales <- sum(causas_inic_ppales, na.rm = T)
          causas_inic_noppales <- sum(causas_inic_noppales, na.rm = T)
          causas_archivadas <- sum(causas_archivadas, na.rm = T)

          result <- OperationResult$new()
          if (nrow(tbs_prim_CADRIA1L) == 0){
            df <- data.frame()
            attr(df, "empty") <- TRUE
            result$addResult(df)
            return(result)
          }

          colnames(tbs_prim_CADRIA1L) <- try(c("nro","caratula","tproc",
                                               "as","ccon","finicio",
                                               "fdesp","fvenc","fres",
                                               "tres","mat",
                                               "justiciables","reccap"))

          calidad_primaria <- vector()
          a <- try(all(tbs_prim_CADRIA1L$as %in% c("A", "S", "a", "s", NA)))
          if (checkWarn(a)){
            result$addWarning("Auto o Sentencia", "Existen Causas no declarando
                                      A (Auto) ó S (Sentencia).")
          }
          b <- try(all(1 == nchar(tbs_prim_CADRIA1L$as[!is.na(tbs_prim_CADRIA1L$as)])))
          if (checkWarn(b)){
            result$addWarning("Auto o Sentencia", "Existen Causas declarando
                                            A (Auto) ó S (Sentencia) con más de dos caracteres.")
          }
          c <- try(all(is.numeric(tbs_prim_CADRIA1L$tres)))
          if (checkWarn(c)){
            result$addWarning("Tipo de Resolución", "Existen Causas declarando
                                            un dato no numérico en Tipo de Resolución.")
          }
          # Validacion de fechas
          tbs_prim_CADRIA1L$fdesp <-
            as.Date(tbs_prim_CADRIA1L$fdesp, format="%d/%m/%Y")
          tbs_prim_CADRIA1L$fvenc <-
            as.Date(tbs_prim_CADRIA1L$fvenc, format="%d/%m/%Y")
          d <- try(all(tbs_prim_CADRIA1L$fdesp <= tbs_prim_CADRIA1L$fvenc))
          if (checkWarn(d)){
            result$addWarning("Fecha de Despacho", "Existen Causas con Fecha
                              de Despacho mayor a la Fecha de Vencimiento 1.")
          }
          # Valida promedio de los no NAs superior o igual a 80% de casos informados
          e <- try(mean(!is.na(tbs_prim_CADRIA1L$as))) >= 0.80
          if (checkWarn(e)){
            result$addWarning("Auto o Sentencia", "La cantidad de Causas
                              declarando A (Auto) ó S (Sentencia) no alcanza
                              el 80% -valor mínimo aceptable-.")
          }
          f <- try(anyDuplicated(select(tbs_prim_CADRIA1L, nro, caratula,
                                        tproc, fdesp)) == 0)
          if (checkWarn(f)){
            result$addWarning("Causas Duplicadas", "Existen Causas Duplicadas
                          con iguales valores en 'Número de Carátula',
                          'Carátula', 'Tipo de Proceso' y 'Fecha de Despacho.")
          }
          calidad_primaria <- round(sum(a, b, c, d, e, f, na.rm = T)/6, 2)
          calidad_primaria


          # Preparar tbs primarias: Asignar clase a columnas fechas, Agregar columna de
          # mes de dictado de resolución. Convertir a mayÃºsculas as
          tbs_prim_CADRIA1L$finicio <-
            as.Date(tbs_prim_CADRIA1L$finicio, format="%d/%m/%Y")
          tbs_prim_CADRIA1L$fdesp <-
            as.Date(tbs_prim_CADRIA1L$fdesp, format="%d/%m/%Y")
          tbs_prim_CADRIA1L$fvenc <-
            as.Date(tbs_prim_CADRIA1L$fvenc, format="%d/%m/%Y")
          tbs_prim_CADRIA1L$fres <-
            as.Date(tbs_prim_CADRIA1L$fres, format="%d/%m/%Y")
          tbs_prim_CADRIA1L$mes_res <-
            month(tbs_prim_CADRIA1L$fres)
          tbs_prim_CADRIA1L$as <-
            toupper(tbs_prim_CADRIA1L$as)


          # Creación de funciones
          p_causasadespacho <- function() {
            # Suma causas con fdesp en el mes que se informa sin los NA
            causas_adespacho <- sum(self$withinInterval(tbs_prim_CADRIA1L$fdesp), na.rm = TRUE)
            causas_adespacho
          }

          p_causasresueltas <- function() {
            # Suma causas con resolución en el mes que se informa que no sean medidas
            # para mejor proveer
            no_mmp <- tbs_prim_CADRIA1L$tres != 0 |
              is.na(tbs_prim_CADRIA1L$tres)
            causas_resueltas <- sum(self$withinInterval(tbs_prim_CADRIA1L$fres) &
                                      no_mmp, na.rm = T)
            causas_resueltas
          }

          p_resxsentencia <- function() {
            # Suma sentencias
            no_mmp <- tbs_prim_CADRIA1L$tres != 0 |
              is.na(tbs_prim_CADRIA1L$tres)
            res_xsentencia <- sum(self$withinInterval(tbs_prim_CADRIA1L$fres) &
                                    tbs_prim_CADRIA1L$as == "S" &
                                    no_mmp, na.rm = T)
            res_xsentencia
          }

          p_resxauto <- function() {
            # Suma autos
            no_mmp <- tbs_prim_CADRIA1L$tres != 0 |
              is.na(tbs_prim_CADRIA1L$tres)
            res_xauto <- sum(self$withinInterval(tbs_prim_CADRIA1L$fres) &
                               tbs_prim_CADRIA1L$as == "A" &
                               no_mmp, na.rm = T)
            res_xauto
          }

          p_resxindeter <- function() {
            # Suma resoluciones otraminadas
            no_mmp <- tbs_prim_CADRIA1L$tres != 0 |
              is.na(tbs_prim_CADRIA1L$tres)
            res_xindeter <- sum(self$withinInterval(tbs_prim_CADRIA1L$fres) &
                               !tbs_prim_CADRIA1L$as %in% c("A", "S") &
                               no_mmp, na.rm = T)
            res_xindeter
          }

          p_resccon <- function() {
            # Suma resoluciones en causas con contención
            no_mmp <- tbs_prim_CADRIA1L$tres != 0 |
              is.na(tbs_prim_CADRIA1L$tres)
            res_ccon <- sum(self$withinInterval(tbs_prim_CADRIA1L$fres) &
                              tbs_prim_CADRIA1L$ccon == 1 &
                              no_mmp, na.rm = T)
            res_ccon
          }



          p_res_atermino <- function() {
            # Suma resoluciones con fecha menor o igual a la fecha de 1er. venc que
            # no sean medidas para mejor proveer
            no_mmp <- tbs_prim_CADRIA1L$tres != 0 |
              is.na(tbs_prim_CADRIA1L$tres)
            res_atermino <- sum(tbs_prim_CADRIA1L$fres <=
                                  tbs_prim_CADRIA1L$fvenc &
                                  self$withinInterval(tbs_prim_CADRIA1L$fres) &
                                  no_mmp, na.rm = T)
            res_atermino
          }

          p_res_luego1venc <- function() {
            # Suma resoluciones con fecha mayor a la del vencimiento sin medidas para mejor proveer
            no_mmp <- tbs_prim_CADRIA1L$tres != 0 |
              is.na(tbs_prim_CADRIA1L$tres)
            res_luego1venc <- sum(tbs_prim_CADRIA1L$fres >
                                    tbs_prim_CADRIA1L$fvenc &
                                    self$withinInterval(tbs_prim_CADRIA1L$fres) &
                                    no_mmp, na.rm = T)
            res_luego1venc
          }

          p_prom_durac_inic_sent <- function() {
            # Duración de Procesos desde Fecha de Inicio hasta Sentencia: PROMEDIO
            # en días corridos
            # ADVERTENCIA: dada esta medición quedan excluidos del indicador de
            # duración los procesos sucesorios y ejecutivos
            indice_sent <- tbs_prim_CADRIA1L$as == "S" &
              self$withinInterval(tbs_prim_CADRIA1L$fres)
            durac_inic_sent <-
              mean(as.integer(tbs_prim_CADRIA1L$fres[indice_sent] -
                                tbs_prim_CADRIA1L$finicio[indice_sent]),
                   na.rm = T)
            durac_inic_sent
          }

          p_durac_inic_sent_xproc <- function() {
            # Duración de Procesos desde Fecha de Inicio hasta Sentencia en días
            # corridos.
            # ADVERTENCIA: dada esta medición quedan excluidos del indicador de
            # duración los procesos sucesorios y ejecutivos
            indice_sent <- tbs_prim_CADRIA1L$as == "S" &
              self$withinInterval(tbs_prim_CADRIA1L$fres)
            # Vector tipo de proceso
            vtproc <- tbs_prim_CADRIA1L$tproc[indice_sent]
            # Vector duracion
            vdurac <- tbs_prim_CADRIA1L$fres[indice_sent] -
              tbs_prim_CADRIA1L$finicio[indice_sent]
            # Vecto combinado
            vcomb <- stringr::str_c(vtproc, vdurac, sep = "&")
            vcomb <- noquote(vcomb[!is.na(vcomb)])
            durac_inic_sent_xproc <- stringr::str_c(vcomb, collapse = "#")
            if (purrr::is_empty(durac_inic_sent_xproc)) {
              durac_inic_sent_xproc <- NA
            } else {
              durac_inic_sent_xproc
            }
            durac_inic_sent_xproc
          }

          p_ccausa_ant <- function() {
            # Causa informada con fecha de despacho mÃ¡s antigua: Caratula
            if (length(tbs_prim_CADRIA1L$fdesp > 0)) {
              ccausa_ant <-
                tbs_prim_CADRIA1L[[which.min(tbs_prim_CADRIA1L$fdesp),
                                              "caratula"]]
              ccausa_ant
            } else {
              NA
            }
          }

          p_fcausa_ant <- function() {
            # Causa informada con fecha de despacho mÃ¡s antigua: Fecha
            if (length(tbs_prim_CADRIA1L$fdesp > 0)) {
              fcausa_ant <-
                tbs_prim_CADRIA1L[[which.min(tbs_prim_CADRIA1L$fdesp),
                                              "fdesp"]]
              fcausa_ant <- as.Date(fcausa_ant)
              fcausa_ant
            } else {
              NA
            }
          }

          # crear vectores
          causas_adespacho <- vector()
          causas_resueltas <- vector()
          res_xsentencia <- vector()
          res_xauto <- vector()
          res_xindeter <- vector()
          res_atermino <- vector()
          res_luego1venc <- vector()
          prom_durac_inic_sent <- vector()
          durac_inic_sent_xproc <- vector()
          ccausa_ant <- vector()
          fcausa_ant <- vector()
          res_ccon <- vector()


          # Extracción de indicadores estadísticos.Con "try" para devolver NA ante
          # falla
          causas_adespacho  <- try(p_causasadespacho(), silent = TRUE)
          causas_adespacho
          causas_resueltas <- try(p_causasresueltas(), silent = TRUE)
          causas_resueltas
          res_xsentencia <- try(p_resxsentencia(), silent = TRUE)
          res_xsentencia
          res_xauto <- try(p_resxauto(), silent = TRUE)
          res_xauto
          res_xindeter <- try(p_resxindeter(), silent = TRUE)
          res_xindeter
          res_atermino <- try(p_res_atermino(), silent = TRUE)
          res_atermino
          res_luego1venc <- try(p_res_luego1venc(), silent = TRUE)
          res_luego1venc
          prom_durac_inic_sent <- try(p_prom_durac_inic_sent(),
                                           silent = TRUE)
          prom_durac_inic_sent
          durac_inic_sent_xproc <- try(p_durac_inic_sent_xproc(),
                                            silent = TRUE)
          durac_inic_sent_xproc
          ccausa_ant <- try(p_ccausa_ant(), silent = TRUE)
          ccausa_ant
          fcausa_ant <- try(p_fcausa_ant(), silent = TRUE)
          fcausa_ant
          res_ccon <- try(p_resccon(), silent = TRUE)
          res_ccon

          # Arma df indicadores procesados
          df <- data.frame(
            causas_adespacho,
            causas_resueltas,
            res_xsentencia,
            res_xauto,
            res_xindeter,
            res_atermino,
            res_luego1venc,
            prom_durac_inic_sent,
            durac_inic_sent_xproc,
            ccausa_ant,
            fcausa_ant,
            res_ccon,
            causas_inic_ppales,
            causas_inic_noppales,
            causas_archivadas,
            calidad_primaria)

          # formateando fecha de causa más antigua
          df$fcausa_ant <- as.Date(df$fcausa_ant, origin="1970-01-01")

          # EvalÃºa calidad del informe presentado en base a consistencia de indicadores
          # 1: Causas resueltas == resueltas x S + x A + x otra
          # 2: Causas resoluestas == resueltas a termino + luego1v + luego2v
          #     Como en estas columnas se presentan NA a veces no se puede evaluar la
          #     la función de procesamiento
          # 3: 0 < Causas Iniciadas (ppales + no ppales) + causas archivadas

          regla1 <- df$causas_resueltas == (df$res_xsentencia +
                                                       df$res_xauto +
                                                       df$res_xindeter)
          regla2 <- df$causas_resueltas == (df$res_atermino +
                                                       df$res_luego1venc)
          regla3 <- 0 < (df$causas_inic_ppales + df$causas_inic_noppales +
                           df$causas_archivadas)
          df$calidad <- round(sum(regla1 + regla2 + regla3, na.rm = T)/3, 2)

          # Causas a despacho para el Boletin Oficial-----------------------------
          # Lista de causas a despacho
          adespacho <- data.frame(
            nro =  tbs_prim_CADRIA1L$nro[is.na(tbs_prim_CADRIA1L$fres)],
            caratula = tbs_prim_CADRIA1L$caratula[is.na(tbs_prim_CADRIA1L$fres)],
            fdesp = tbs_prim_CADRIA1L$fdesp[is.na(tbs_prim_CADRIA1L$fres)]
          )

          result$addResult(df)
          result$addResult(adespacho, name="adespacho")


          sentencias_pendientes <- tbs_prim_CADRIA1L %>%
            filter(is.na(fres), as =="S") %>%
            mutate(sentencias_pendientes_vencidas = fvenc < lubridate::int_end(self$interval)) %>%
            arrange(fdesp) %>%
            dplyr::summarise(sentencias_pendientes = n(), sentencias_pendientes_vencidas = sum(sentencias_pendientes_vencidas), s_fdespacho_mas_antigua=fdesp[1], s_catatula_mas_antigua=caratula[1])

          autos_pendientes <- tbs_prim_CADRIA1L %>%
            filter(is.na(fres), as =="A") %>%
            mutate(autos_pendientes_vencidos = fvenc < lubridate::int_end(self$interval)) %>%
            arrange(fdesp) %>%
            dplyr::summarise(autos_pendientes = n(), autos_pendientes_vencidos = sum(autos_pendientes_vencidos), a_fdespacho_mas_antigua=fdesp[1], a_catatula_mas_antigua=caratula[1])

          listado_pendientes <- tbs_prim_CADRIA1L %>%
            filter(is.na(fres)) %>%
            mutate(vencido = fvenc < lubridate::int_end(self$interval)) %>%
            arrange(fdesp)

          pendientes <- cbind(sentencias_pendientes, autos_pendientes)
          pendientes$total_adespacho <- causas_adespacho
          pendientes <- pendientes %>%
            mutate(total_pendientes = nrow(listado_pendientes)) %>%
            mutate(total_pendientes_vencidas = listado_pendientes %>% filter(vencido) %>% nrow()) %>%
            mutate(porcentaje_pendientes = total_pendientes / causas_adespacho )

          result$addResult(pendientes, name="pendientes")
          result$addResult(listado_pendientes, name="listado_pendientes")


          result

        }
      )
)
