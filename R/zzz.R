.onLoad <- function(libname = find.package("apgyeOperationsJusER"),
                    pkgname = "apgyeOperationsJusER") {

  options('dataAccess.filter.entries.vcadr1c' = rlang::expr(table %>%
                                                              filter(is.na(fres) | (dmy(fres) >= data_interval_start & dmy(fres) < data_interval_end)) %>%
                                                              arrange(data_interval_start, as) %>% # para agrupamientos con identidad de casos resuelve quedando S.
                                                              group_by(iep, nro, fdesp, fres) %>% filter(row_number() == n()) %>% ungroup() ))
  options('dataAccess.filter.entries.vcadr2c' = rlang::expr(table %>%
                                                              filter(is.na(fres) | (dmy(fres) >= data_interval_start & dmy(fres) < data_interval_end)) %>%
                                                              arrange(data_interval_start, as) %>% # para agrupamientos con identidad de casos resuelve quedando S.
                                                              group_by(iep, nro, fdesp1) %>% filter(row_number() == n()) %>% ungroup() ))
  options('dataAccess.filter.entries.vcadr3c' = rlang::expr(table %>%
                                                              filter(is.na(fres) | (dmy(fres) >= data_interval_start & dmy(fres) < data_interval_end)) %>%
                                                              arrange(data_interval_start, as) %>% # para agrupamientos con identidad de casos resuelve quedando S.
                                                              group_by(iep, nro, fdesp1) %>% filter(row_number() == n()) %>% ungroup() ))
  options('dataAccess.filter.entries.vcadr1l' = rlang::expr(table %>%
                                                              filter(is.na(fres) | (dmy(fres) >= data_interval_start & dmy(fres) < data_interval_end)) %>%
                                                              arrange(data_interval_start, as) %>% # para agrupamientos con identidad de casos resuelve quedando S.
                                                              group_by(iep, nro, fdesp, fres) %>% filter(row_number() == n()) %>% ungroup() ))
  options('dataAccess.filter.entries.vcadr1lyc' = rlang::expr(table %>%
                                                              filter(is.na(fres) | (dmy(fres) >= data_interval_start & dmy(fres) < data_interval_end)) %>%
                                                              arrange(data_interval_start, as) %>% # para agrupamientos con identidad de casos resuelve quedando S.
                                                              group_by(iep, nro, fdesp, fres) %>% filter(row_number() == n()) %>% ungroup() ))
  options('dataAccess.filter.entries.vcadr2l' = rlang::expr(table %>%
                                                              filter(is.na(fres) | (dmy(fres) >= data_interval_start & dmy(fres) < data_interval_end)) %>%
                                                              arrange(data_interval_start, as) %>% # para agrupamientos con identidad de casos resuelve quedando S.
                                                              group_by(iep, nro, fdesp1) %>% filter(row_number() == n()) %>% ungroup() ))
  options('dataAccess.filter.entries.vcadr3l' = rlang::expr(table %>%
                                                              filter(is.na(fres) | (dmy(fres) >= data_interval_start & dmy(fres) < data_interval_end)) %>%
                                                              arrange(data_interval_start, as) %>% # para agrupamientos con identidad de casos resuelve quedando S.
                                                              group_by(iep, nro, fdesp1) %>% filter(row_number() == n()) %>% ungroup() ))
  options('dataAccess.filter.entries.vaudic' = rlang::expr(table %>%
                                                             filter((dmy(fea) >= data_interval_start & dmy(fea) < data_interval_end)) %>%
                                                             arrange(data_interval_start, fea) # ordenamiento de la consulta
                                                           #group_by(iep, nro, fdesp1) %>% filter(row_number() == n()) %>% ungroup()
  ))
  options('dataAccess.filter.entries.AUDIL_tbs_prim_AUDIL' = rlang::expr(table %>%
                                                             filter((dmy(fea) >= data_interval_start & dmy(fea) < data_interval_end)) %>%
                                                             arrange(data_interval_start, fea) # ordenamiento de la consulta
                                                           #group_by(iep, nro, fdesp1) %>% filter(row_number() == n()) %>% ungroup()
  ))
  options('dataAccess.filter.entries.CRMED_tbs_prim_CRMED' = rlang::expr(table %>%
                                                               filter(is.na(fres) | (dmy(fres) >= data_interval_start & dmy(fres) < data_interval_end)) %>%
                                                               group_by(iep, nro, caratula, tproc, fres) %>% filter(row_number() == n()) %>% ungroup()
  ))
  options('dataAccess.filter.entries.vcadrcad' = rlang::expr(table %>%
                                                              filter(is.na(fres) | (dmy(fres) >= data_interval_start & dmy(fres) < data_interval_end)) %>%
                                                              arrange(data_interval_start, as) %>% # para agrupamientos con identidad de casos resuelve quedando S.
                                                              group_by(iep, nro, fdesp) %>% filter(row_number() == n()) %>% ungroup() ))

  options('dataAccess.process.vresolpg' = RESOLPG_v2)
  options('dataAccess.process.vcadr1c' = CADR1C_v2)
  options('dataAccess.process.vcadr2c' = CADR2C)
  options('dataAccess.process.vcadr3c' = CADR3C)
  options('dataAccess.process.vcadr1l' = CADR1L_v2)
  options('dataAccess.process.vcadr1lyc' = CADR1L_v2)
  options('dataAccess.process.vcadr2l' = CADR2L)
  options('dataAccess.process.vcadr3l' = CADR3L)
  options('dataAccess.process.vaudic' = AUDIC_v2)
  options('dataAccess.process.AUDIL_tbs_prim_AUDIL' = AUDIL)
  options('dataAccess.process.vaudip' = AUDIP_v2)
  options('dataAccess.process.vaudiptj' = AUDIPTJ_v2)
  options('dataAccess.process.AUDIF_tbs_prim_AUDIF' = AUDIF)
  options('dataAccess.process.vcamov' = CAMOV_v2)
  options('dataAccess.process.vcinc1c' = CINC1C_v2)
  options('dataAccess.process.vcinc1fc' = CINC1FC_v2)
  options('dataAccess.process.vcinc1l' = CINC1L_v2)
  options('dataAccess.process.CICAM_tbs_prim_CICAM' = CICAM)
  options('dataAccess.process.CRMED_tbs_prim_CRMED' = CRMED)
  options('dataAccess.process.vcarch' = CARCH_v2)
  options('dataAccess.process.vapsatab' = AP_SAT_AB)
  options('dataAccess.process.vapsatus' = AP_SAT_US)
  options('dataAccess.process.vavcsatab' = AVC_SAT_AB)
  options('dataAccess.process.vavcsatus' = AVC_SAT_US)
  options('dataAccess.process.vcadrcad' = CADRCAD_v2)

  options('dataAccess.tableMap.personal_planta' = "GHSTRD_tbs_prim_GHSTRD_planta")
  options('dataAccess.tableMap.personal_planta_ocupada' = "GHSTRD_tbs_prim_GHSTRD_planta_ocupada")
  options('dataAccess.tableMap.personal_personas' = "GHSTRD_tbs_prim_GHSTRD_personas")
  options('dataAccess.tableMap.personal_licencias' = "GHSTRD_tbs_prim_GHSTRD_licencias")

  options('dataAccess.tableMap.CITIPP23' = "CITIPP23_primaryTable")
  options('dataAccess.tableMap.RESOLPG' = "vresolpg")
  options('dataAccess.tableMap.AUDIP' = "vaudip")
  options('dataAccess.tableMap.AUDIC' = "vaudic")
  options('dataAccess.tableMap.AUDIPTJ' = "vaudiptj")
  options('dataAccess.tableMap.CADR1C' = "vcadr1c")
  options('dataAccess.tableMap.CADR2C'= "vcadr2c")
  options('dataAccess.tableMap.CADR3C' = "vcadr3c")
  options('dataAccess.tableMap.CADR1L' = "vcadr1l")
  options('dataAccess.tableMap.CADR1LYC' = "vcadr1lyc")
  options('dataAccess.tableMap.CADR2L' = "vcadr2l")
  options('dataAccess.tableMap.CADR3L' = "vcadr3l")
  options('dataAccess.tableMap.CINC1C' = "vcinc1c")
  options('dataAccess.tableMap.CINC1FC' = "vcinc1fc")
  options('dataAccess.tableMap.CINC1L' = "vcinc1l")
  options('dataAccess.tableMap.CARCH' = "vcarch")
  options('dataAccess.tableMap.CAMOV' = "vcamov")
  options('dataAccess.tableMap.CICAM' = "CICAM_tbs_prim_CICAM")
  options('dataAccess.tableMap.CRMED' = "CRMED_tbs_prim_CRMED")
  options('dataAccess.tableMap.AP_SAT_AB' = "vapsatab")
  options('dataAccess.tableMap.AP_SAT_US' = "vapsatus")
  options('dataAccess.tableMap.AVC_SAT_AB' = "vavcsatab")
  options('dataAccess.tableMap.AVC_SAT_US' = "vavcsatus")
  options('dataAccess.tableMap.CADRCAD' = "vcadrcad")

  options("dataAccess.filter.entries.GHSTRD_tbs_prim_GHSTRD_planta" = rlang::expr(table %>% group_by(idjurisdiccion, idlocalidad, idorganismo_padre, idorganismo) %>% filter(row_number() == n()) ))
  options("dataAccess.filter.entries.GHSTRD_tbs_prim_GHSTRD_planta_ocupada" = rlang::expr(table %>% group_by(idjurisdiccion, idlocalidad, idorganismo_padre, idorganismo, idagente) %>% filter(row_number() == n()) ))
  options("dataAccess.filter.entries.GHSTRD_tbs_prim_GHSTRD_personas" = rlang::expr(table %>% group_by(idagente) %>% filter(row_number() == n()) ))
  options("dataAccess.filter.entries.GHSTRD_tbs_prim_GHSTRD_licencias" = rlang::expr(table %>% group_by(idagente) %>% filter(row_number() == n()) ))

  # Mapeo de nombres nuevo Nombre = Todos los sinónimos de ése nombre.
  options("dataAccess.filterieps" = list( 'jdofam0100con' = c('jdofam0100con', 'jdofam0101con'),
                                          'jdofam0200con' = c('jdofam0200con', 'jdofam0201con'),
                                          'jdofam0300con' = c('jdofam0300con', 'jdofam0102con', 'jdofam0202con'),
                                          'jdofam0100gch' = c('jdofam0100gch', 'jdofam0001gch'),
                                          'jdofam0200gch' = c('jdofam0200gch', 'jdofam0002gch'),
                                          'jdofam0200uru' = c('jdofam0200uru', 'jdofam0001uru', 'jdofam0002uru')
                                          ))

}
